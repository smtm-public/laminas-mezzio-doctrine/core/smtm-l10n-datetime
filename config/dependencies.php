<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime;

use Smtm\Base\Application\Service\ApplicationService\Factory\DbConfigurableEntityManagerNameServiceDelegator;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\L10n\DateTime\Context\UtcOffset\Application\Service\UtcOffsetService;
use Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Service\TimeZoneService;
use Smtm\L10n\DateTime\Factory\L10nDateTimeConfigAwareDelegator;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();

                $applicationServicePluginManager->addDelegator(
                    UtcOffsetService::class,
                    L10nDateTimeConfigAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    UtcOffsetService::class,
                    DbConfigurableEntityManagerNameServiceDelegator::class
                );

                $applicationServicePluginManager->addDelegator(
                    TimeZoneService::class,
                    L10nDateTimeConfigAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    TimeZoneService::class,
                    DbConfigurableEntityManagerNameServiceDelegator::class
                );

                return $applicationServicePluginManager;
            }
        ],
    ],
];
