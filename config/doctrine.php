<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime;

return [
    'orm' => [
        'mapping' => [
            'paths' => [
                \Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Domain\TimeZone::class =>
                    __DIR__ . '/../src/Context/UtcOffset/Context/TimeZone/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\L10n\DateTime\Context\UtcOffset\Domain\UtcOffset::class =>
                    __DIR__ . '/../src/Context/UtcOffset/Infrastructure/Doctrine/Orm/Mapping',

            ],
        ],
    ],
];
