<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_l10n_datetime',
    ],

    'migrations_paths' => [
        'Smtm\L10n\DateTime\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
