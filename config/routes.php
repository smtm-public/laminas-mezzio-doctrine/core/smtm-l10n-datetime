<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-l10n-datetime')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-l10n-datetime'
    );
    $dotenv->load();
}

$exposedRoutes = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_L10N_DATETIME_EXPOSED_ROUTES', '[]'),
    true
);
$exposedRoutes = array_combine($exposedRoutes, $exposedRoutes);

$routes = [
    'smtm.l10n.datetime.utc-offset.time-zone.read' => [
        'path' => '/l10n/datetime/utc-offset/time-zone/{uuid}',
        'method' => 'get',
        'middleware' => Context\UtcOffset\Context\TimeZone\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.l10n.datetime.utc-offset.time-zone.index' => [
        'path' => '/l10n/datetime/utc-offset/time-zone',
        'method' => 'get',
        'middleware' => Context\UtcOffset\Context\TimeZone\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];

return array_intersect_key($routes, $exposedRoutes);
