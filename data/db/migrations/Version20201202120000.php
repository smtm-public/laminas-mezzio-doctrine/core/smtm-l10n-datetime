<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\UuidStringColumnAndUniqueIndexMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use UuidStringColumnAndUniqueIndexMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createL10nDateTimeUtcOffsetTable($schema);
    }

    public function createL10nDateTimeUtcOffsetTable(Schema $schema): void
    {
        $l10nDateTimeUtcOffsetTable = $schema->createTable('l10n_datetime_utc_offset');
        $l10nDateTimeUtcOffsetTable->addColumn(
            'id',
            Types::INTEGER,
            ['notNull' => true, 'autoincrement' => true]
        );
        $l10nDateTimeUtcOffsetTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($l10nDateTimeUtcOffsetTable);
        $l10nDateTimeUtcOffsetTable->addColumn(
            'hours',
            Types::STRING,
            ['length' => 16, 'notNull' => true]
        );
        $l10nDateTimeUtcOffsetTable->addUniqueIndex(
            ['hours'],
            'idx_unq_' . $l10nDateTimeUtcOffsetTable->getName() . '_hours'
        );
        $l10nDateTimeUtcOffsetTable->addColumn('seconds', Types::INTEGER, ['notNull' => true]);
        $l10nDateTimeUtcOffsetTable->addUniqueIndex(
            ['seconds'],
            'idx_unq_' . $l10nDateTimeUtcOffsetTable->getName() . '_seconds'
        );
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('l10n_datetime_utc_offset');
    }
}
