<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120001 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->populateL10nDateTimeUtcOffsetTable($schema);
    }

    public function populateL10nDateTimeUtcOffsetTable(Schema $schema): void
    {
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 1,
            'uuid' => '88ab32ab-512c-411b-8112-ae931bc9a0c9',
            'hours' => '-12:00',
            'seconds' => -43200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 2,
            'uuid' => '94ee2d13-ec2a-416e-978b-ef819eca2385',
            'hours' => '-11:00',
            'seconds' => -39600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 3,
            'uuid' => '8a18af94-1268-4ba7-bfd0-01a4329ac33d',
            'hours' => '-10:00',
            'seconds' => -36000,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 4,
            'uuid' => '4293b71c-f068-4259-9fe9-f49b0151c953',
            'hours' => '-09:30',
            'seconds' => -34200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 5,
            'uuid' => '638f3c0a-f545-47c0-b2bf-45d29854bd21',
            'hours' => '-09:00',
            'seconds' => -32400,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 6,
            'uuid' => '666071fc-e0c6-46a0-91c4-2a6abf943b4e',
            'hours' => '-08:00',
            'seconds' => -28800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 7,
            'uuid' => '4e8626fc-2fae-4081-8910-2155363f8515',
            'hours' => '-07:00',
            'seconds' => -25200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 8,
            'uuid' => '3eb10629-ed61-4bb5-b97c-b0da2746f608',
            'hours' => '-06:00',
            'seconds' => -21600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 9,
            'uuid' => '94abae25-5815-4b70-bb5f-ba30893cc17b',
            'hours' => '-05:00',
            'seconds' => -18000,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 10,
            'uuid' => '5356fe97-3750-45eb-84a4-b759641f2d8e',
            'hours' => '-04:00',
            'seconds' => -14400,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 11,
            'uuid' => 'a9fbdfd7-0946-4dfc-be46-e709257eb75d',
            'hours' => '-03:30',
            'seconds' => -12600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 12,
            'uuid' => '7b1f8d01-dd8e-4506-ab9a-6a54b4fd7cc2',
            'hours' => '-03:00',
            'seconds' => -10800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 13,
            'uuid' => 'd31094af-cf45-42df-828e-5ed80bc08cc8',
            'hours' => '-02:30',
            'seconds' => -9000,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 14,
            'uuid' => '50f77091-78fd-4711-9f08-d8fb090bb815',
            'hours' => '-02:00',
            'seconds' => -7200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 15,
            'uuid' => '176a83fc-ee63-4e81-80f2-317db946ff3d',
            'hours' => '-01:00',
            'seconds' => -3600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 16,
            'uuid' => 'cc3bbfed-9461-4166-8a1d-a04d00c874d0',
            'hours' => '+00:00',
            'seconds' => 0,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 17,
            'uuid' => '88139d9b-76b3-40c6-b1c7-75763a07071d',
            'hours' => '+01:00',
            'seconds' => 3600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 18,
            'uuid' => '0dd8a7fd-dc1b-42ae-858c-95c5b3976582',
            'hours' => '+02:00',
            'seconds' => 7200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 19,
            'uuid' => '051018db-29ed-4b96-a9ca-484c0e43b9eb',
            'hours' => '+03:00',
            'seconds' => 10800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 20,
            'uuid' => '9a093046-b49c-4b12-9209-d5ee0206191e',
            'hours' => '+03:30',
            'seconds' => 12600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 21,
            'uuid' => '0e2f4a55-493b-4d4b-834c-18b507aeafec',
            'hours' => '+04:00',
            'seconds' => 14400,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 22,
            'uuid' => 'abd54ddc-fffc-4aa1-aad4-e687218e2f5c',
            'hours' => '+04:30',
            'seconds' => 16200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 23,
            'uuid' => '0237e0c7-e49a-4ce7-82a2-519dc38d0b63',
            'hours' => '+05:00',
            'seconds' => 18000,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 24,
            'uuid' => '9cea3b42-039e-41e5-a173-fc5591ec2700',
            'hours' => '+05:30',
            'seconds' => 19800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 25,
            'uuid' => '8712ef14-bb7b-4aab-9b80-7052ebbb1e50',
            'hours' => '+05:45',
            'seconds' => 20700,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 26,
            'uuid' => '4e1dbcda-9813-424a-9a1e-6c5618d851f9',
            'hours' => '+06:00',
            'seconds' => 21600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 27,
            'uuid' => '8c4eb83c-c35a-4cca-abf4-e047b0467721',
            'hours' => '+06:30',
            'seconds' => 23400,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 28,
            'uuid' => '2b9b616f-e0b9-4c0e-8884-34f01266004f',
            'hours' => '+07:00',
            'seconds' => 25200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 29,
            'uuid' => 'd4d51715-ddda-42cb-889e-65ca27fb5d78',
            'hours' => '+08:00',
            'seconds' => 28800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 30,
            'uuid' => '96e84628-c9bf-498c-8883-4b35b746c5ac',
            'hours' => '+08:45',
            'seconds' => 29700,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 31,
            'uuid' => '431ed5b1-b14e-4740-9685-ea404d723aee',
            'hours' => '+09:00',
            'seconds' => 32400,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 32,
            'uuid' => '9432b049-9801-47f0-878a-52e66343de3a',
            'hours' => '+09:30',
            'seconds' => 34200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 33,
            'uuid' => '20f00981-78ff-46d1-8a4a-f9d570c4b8a5',
            'hours' => '+10:00',
            'seconds' => 36000,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 34,
            'uuid' => '81401730-2cd4-49f3-9c04-f22e91e3cd15',
            'hours' => '+10:30',
            'seconds' => 37800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 35,
            'uuid' => '9b74b213-9215-44ff-b888-e4dec1d0e17d',
            'hours' => '+11:00',
            'seconds' => 39600,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 36,
            'uuid' => 'd4f6e72e-94cb-4e42-92e9-bb40cfeb9e38',
            'hours' => '+12:00',
            'seconds' => 43200,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 37,
            'uuid' => '73d9f4ee-89c5-4b58-8d3d-f26b9bf85b45',
            'hours' => '+12:45',
            'seconds' => 45900,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 38,
            'uuid' => 'bb17c0db-9646-4eb8-9859-7b0342b5626c',
            'hours' => '+13:00',
            'seconds' => 46800,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 39,
            'uuid' => 'e50ccbe3-335f-4a21-b744-1a55f40d5433',
            'hours' => '+13:45',
            'seconds' => 49500,
        ]);
        $this->connection->insert('l10n_datetime_utc_offset', [
            'id' => 40,
            'uuid' => '58915d1e-4cdf-4f70-8eed-05c7a9f27da3',
            'hours' => '+14:00',
            'seconds' => 50400,
        ]);
    }

    public function down(Schema $schema): void
    {

    }
}
