<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Smtm\Base\Infrastructure\Doctrine\Migration\UuidStringColumnAndUniqueIndexMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120002 extends AbstractMigration
{

    use CommonMigrationTrait,
        UuidStringColumnAndUniqueIndexMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createL10nDateTimeTimeZoneTable($schema);
    }

    public function createL10nDateTimeTimeZoneTable(Schema $schema): void
    {
        $l10nDateTimeTimeZoneTable = $schema->createTable('l10n_datetime_time_zone');
        $l10nDateTimeTimeZoneTable->addColumn(
            'id',
            Types::INTEGER,
            ['notNull' => true, 'autoincrement' => true]
        );
        $l10nDateTimeTimeZoneTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($l10nDateTimeTimeZoneTable);
        $l10nDateTimeTimeZoneTable->addColumn(
            'r_name',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $l10nDateTimeTimeZoneTable->addUniqueIndex(
            ['r_name'],
            'idx_unq_' . $l10nDateTimeTimeZoneTable->getName() . '_r_name'
        );
        $l10nDateTimeTimeZoneTable->addColumn(
            'r_status',
            Types::SMALLINT,
            ['notNull' => true]
        );
        $l10nDateTimeTimeZoneTable->addColumn(
            'links_to_l10n_datetime_time_zone_id',
            Types::INTEGER,
            ['notNull' => false]
        );
        $l10nDateTimeTimeZoneTable->addIndex(
            ['links_to_l10n_datetime_time_zone_id'],
            'idx_links_to_l10n_datetime_time_zone_id'
        );
        $l10nDateTimeTimeZoneTable->addForeignKeyConstraint(
            'l10n_datetime_time_zone',
            ['links_to_l10n_datetime_time_zone_id'],
            ['id'],
            [],
            'fk_' . $l10nDateTimeTimeZoneTable->getName() . '_lt_l10n_datetime_utc_offset_seconds'
        );
        $l10nDateTimeTimeZoneTable->addColumn(
            'l10n_datetime_utc_offset_seconds',
            Types::INTEGER,
            ['notNull' => false]
        );
        $l10nDateTimeTimeZoneTable->addIndex(
            ['l10n_datetime_utc_offset_seconds'],
            'idx_l10n_datetime_utc_offset_seconds'
        );
        $l10nDateTimeTimeZoneTable->addForeignKeyConstraint(
            'l10n_datetime_utc_offset',
            ['l10n_datetime_utc_offset_seconds'],
            ['seconds'],
            [],
            'fk_' . $l10nDateTimeTimeZoneTable->getName() . '_l10n_datetime_utc_offset_seconds'
        );
        $l10nDateTimeTimeZoneTable->addColumn(
            'dst_l10n_datetime_utc_offset_seconds',
            Types::INTEGER,
            ['notNull' => true]
        );
        $l10nDateTimeTimeZoneTable->addIndex(
            ['dst_l10n_datetime_utc_offset_seconds'],
            'idx_dst_l10n_datetime_utc_offset_seconds'
        );
        $l10nDateTimeTimeZoneTable->addForeignKeyConstraint(
            'l10n_datetime_utc_offset',
            ['dst_l10n_datetime_utc_offset_seconds'],
            ['seconds'],
            [],
            'fk_' . $l10nDateTimeTimeZoneTable->getName() . '_dst_l10n_datetime_utc_offset_seconds'
        );
        $l10nDateTimeTimeZoneTable->addColumn(
            'i18n_territorial_division_code_iso3166_alpha2',
            Types::STRING,
            ['length' => 2, 'notNull' => false]
        );
        $l10nDateTimeTimeZoneTable->addIndex(
            ['i18n_territorial_division_code_iso3166_alpha2'],
            'idx_i18n_territorial_division_code_iso3166_alpha2'
        );
        $l10nDateTimeTimeZoneTable->addForeignKeyConstraint(
            'i18n_territorial_division',
            ['i18n_territorial_division_code_iso3166_alpha2'],
            ['code_iso3166_alpha2'],
            [],
            'fk_' . $l10nDateTimeTimeZoneTable->getName() . '_i18n_td_code_iso3166_alpha2'
        );
        $this->addNotArchivedSmallintColumnAndIndex($l10nDateTimeTimeZoneTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($l10nDateTimeTimeZoneTable);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('l10n_datetime_time_zone');
    }
}
