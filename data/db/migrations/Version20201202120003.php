<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120003 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->populateL10nDateTimeTimeZoneTable($schema);
    }

    public function populateL10nDateTimeTimeZoneTable(Schema $schema): void
    {
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 1,
            'uuid' => 'b4cf58e3-7656-796a-57fd-f120d2029994',
            'r_name' => 'Africa/Abidjan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 2,
            'uuid' => '887cf6a8-9b8d-6b47-fa0f-bcd15f2ed215',
            'r_name' => 'Africa/Accra',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 3,
            'uuid' => 'aff0aa70-a842-f250-96d3-5a0ef1cd725e',
            'r_name' => 'Africa/Addis_Ababa',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ET',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 4,
            'uuid' => '6a214aa4-1730-6abd-e5e1-6c9d48a9b14a',
            'r_name' => 'Africa/Algiers',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 5,
            'uuid' => 'a9db03fd-9dd7-1941-d7fa-dbfc4765a27d',
            'r_name' => 'Africa/Asmara',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ER',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 6,
            'uuid' => 'f98bfce3-b5a9-258d-4cf2-95f7402513d2',
            'r_name' => 'Africa/Asmera',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ER',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 7,
            'uuid' => 'c9a4b585-83f1-27f0-3666-9af6223e0c19',
            'r_name' => 'Africa/Bamako',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ML',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 8,
            'uuid' => '2c5dd8a2-db19-506d-0397-c63395e1bd3f',
            'r_name' => 'Africa/Bangui',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 9,
            'uuid' => '0db905da-93ff-517b-2172-b6cbd6704ea0',
            'r_name' => 'Africa/Banjul',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 10,
            'uuid' => '6fab3a7f-4fce-1050-667b-182585f60b66',
            'r_name' => 'Africa/Bissau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 11,
            'uuid' => 'd6a262be-955b-9536-40e9-8038b56ef0c9',
            'r_name' => 'Africa/Blantyre',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 12,
            'uuid' => '45a9a932-50ae-b4c1-2fdd-dfc13880ed07',
            'r_name' => 'Africa/Brazzaville',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 13,
            'uuid' => '8e723164-50af-cdb0-80fa-49afcae50758',
            'r_name' => 'Africa/Bujumbura',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 14,
            'uuid' => '812397e1-72ce-b8e1-5b81-8d70f6b7d762',
            'r_name' => 'Africa/Cairo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'EG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 15,
            'uuid' => '35442908-10ba-82a1-ffe4-b147c34ede4e',
            'r_name' => 'Africa/Casablanca',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 16,
            'uuid' => '7e96a146-96b0-9433-ab05-2f4edb7466bb',
            'r_name' => 'Africa/Ceuta',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ES',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 17,
            'uuid' => 'd06fad20-dd37-cb81-9eb0-129a677cc678',
            'r_name' => 'Africa/Conakry',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 18,
            'uuid' => 'c61f6132-2667-3a7e-fb81-32309674a8a2',
            'r_name' => 'Africa/Dakar',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 19,
            'uuid' => '8c941e18-c01d-8b7e-52f4-156f9616a5ac',
            'r_name' => 'Africa/Dar_es_Salaam',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 20,
            'uuid' => '267ae6f4-c4a5-6881-d486-298814c4239c',
            'r_name' => 'Africa/Djibouti',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DJ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 21,
            'uuid' => 'c1c39511-c34c-d1ed-b3b6-5b00376887b1',
            'r_name' => 'Africa/Douala',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 22,
            'uuid' => '276426da-f53c-9545-8a49-a1eccee94134',
            'r_name' => 'Africa/El_Aaiun',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'EH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 23,
            'uuid' => 'ae488297-56f7-eeec-1f4e-ffa801f7dc11',
            'r_name' => 'Africa/Freetown',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 24,
            'uuid' => '13d8b7fe-c52a-af7b-ccbb-ffa5ed8b85a4',
            'r_name' => 'Africa/Gaborone',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 25,
            'uuid' => '2f2a5772-970c-336f-9033-e67259970025',
            'r_name' => 'Africa/Harare',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ZW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 26,
            'uuid' => '3af27e27-bf3f-4dfd-c4ec-f6c0e0916bad',
            'r_name' => 'Africa/Johannesburg',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ZA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 27,
            'uuid' => '1389d556-5b67-c79d-54fb-c9977ac4c794',
            'r_name' => 'Africa/Juba',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 28,
            'uuid' => '08f5474d-e2e5-88e5-350d-4892557337c2',
            'r_name' => 'Africa/Kampala',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 29,
            'uuid' => 'c4d1508f-7bc2-acda-1efb-be39f3bfaba7',
            'r_name' => 'Africa/Khartoum',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 30,
            'uuid' => '231d8a49-246f-a2ef-8b2c-3f4649b503ac',
            'r_name' => 'Africa/Kigali',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 31,
            'uuid' => '004a0681-3d16-de5f-5427-de52850ca550',
            'r_name' => 'Africa/Kinshasa',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 32,
            'uuid' => '079a00bd-a6e6-7e9f-5daa-9bf64ec03053',
            'r_name' => 'Africa/Lagos',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 33,
            'uuid' => 'b02506c7-648b-c631-d8ba-cafd905dd60a',
            'r_name' => 'Africa/Libreville',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 34,
            'uuid' => '3d3f667b-4f3a-0dff-b60f-dbf649b47edc',
            'r_name' => 'Africa/Lome',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 35,
            'uuid' => 'd54a4699-23db-1f2d-2217-76e1ec50b854',
            'r_name' => 'Africa/Luanda',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 36,
            'uuid' => 'c0be1fe5-a5c6-2e8a-610a-a14c8d331dd7',
            'r_name' => 'Africa/Lubumbashi',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 37,
            'uuid' => '4d2a8b39-8668-5faa-1f34-9b4a0977ed61',
            'r_name' => 'Africa/Lusaka',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ZM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 38,
            'uuid' => 'c8a02e6b-8f3c-295c-5a39-3af768a16fb4',
            'r_name' => 'Africa/Malabo',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 39,
            'uuid' => '6e271d30-50db-ddc6-a7f4-6f66b3a973a7',
            'r_name' => 'Africa/Maputo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 40,
            'uuid' => 'c80fbc09-73a5-50c5-885f-6909c0661230',
            'r_name' => 'Africa/Maseru',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 41,
            'uuid' => 'bc9c2d4d-38d9-ea6b-1541-23be3817f49c',
            'r_name' => 'Africa/Mbabane',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 42,
            'uuid' => '396d2148-1375-5fc5-0ac3-f36f00fb8e8c',
            'r_name' => 'Africa/Mogadishu',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 43,
            'uuid' => '73be9ec2-c5fb-bbc9-d140-4f095cf36f87',
            'r_name' => 'Africa/Monrovia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 44,
            'uuid' => '01d23cbf-9856-b662-f709-22a8b29aeb74',
            'r_name' => 'Africa/Nairobi',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 45,
            'uuid' => '27885db4-9a5b-39d4-6539-a6d44ee881b5',
            'r_name' => 'Africa/Ndjamena',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 46,
            'uuid' => '4e78f32e-b89d-a84d-f7d8-d47495031aea',
            'r_name' => 'Africa/Niamey',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 47,
            'uuid' => 'af287d7e-1842-da62-3ab3-f7f4abd4244d',
            'r_name' => 'Africa/Nouakchott',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 48,
            'uuid' => 'a470e921-f8a6-c025-711c-724c135df2a2',
            'r_name' => 'Africa/Ouagadougou',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 49,
            'uuid' => 'a3b09f9f-cfab-b717-ce9c-ef29df00f541',
            'r_name' => 'Africa/Porto-Novo',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BJ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 50,
            'uuid' => 'e76e0134-99fa-28a8-588f-de70edf1f12c',
            'r_name' => 'Africa/Sao_Tome',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ST',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 51,
            'uuid' => 'ff9880ea-cb44-e03c-3b43-81f9b4af46a7',
            'r_name' => 'Africa/Timbuktu',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ML',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 52,
            'uuid' => 'ee1d3f4f-bd63-3931-7b19-02467948b80d',
            'r_name' => 'Africa/Tripoli',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 53,
            'uuid' => 'acb8c44a-1bab-ebbf-a7b7-2fa3a59eefd4',
            'r_name' => 'Africa/Tunis',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 54,
            'uuid' => 'd0001138-d4c2-2ac0-2bee-552ee2b709f1',
            'r_name' => 'Africa/Windhoek',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 55,
            'uuid' => '1b6c3791-471b-d9c8-fdce-2a2c72a149f0',
            'r_name' => 'America/Adak',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 56,
            'uuid' => 'b67ff895-76c0-80f3-41fc-cec55e2923ae',
            'r_name' => 'America/Anchorage',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 57,
            'uuid' => '2307ff1e-611a-079d-48fe-c503a4137603',
            'r_name' => 'America/Anguilla',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 58,
            'uuid' => '546b744a-6b88-f839-0588-379b4a65ee60',
            'r_name' => 'America/Antigua',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 59,
            'uuid' => '256a3531-d5d0-31b8-6fe9-0c4eefe48866',
            'r_name' => 'America/Araguaina',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 60,
            'uuid' => '1923ec22-52fb-e8b3-269d-12c9752fe4b1',
            'r_name' => 'America/Argentina/Buenos_Aires',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 61,
            'uuid' => '741b2a6c-24f7-1882-1f1d-73bc8356d478',
            'r_name' => 'America/Argentina/Catamarca',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 62,
            'uuid' => '77eea33c-d83d-c485-0534-430690ead31a',
            'r_name' => 'America/Argentina/ComodRivadavia',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 63,
            'uuid' => '76bc5073-a804-f326-c5b9-f9fa6081b804',
            'r_name' => 'America/Argentina/Cordoba',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 64,
            'uuid' => '6079ec7a-c577-4afa-54bc-e318110018d7',
            'r_name' => 'America/Argentina/Jujuy',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 65,
            'uuid' => '6ed42d35-6138-d07b-f287-0d19626cede6',
            'r_name' => 'America/Argentina/La_Rioja',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 66,
            'uuid' => 'bcb06008-e6a9-b11f-d9a6-7de92b667aad',
            'r_name' => 'America/Argentina/Mendoza',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 67,
            'uuid' => 'a07c9629-feda-c4f5-5fd1-694225621088',
            'r_name' => 'America/Argentina/Rio_Gallegos',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 68,
            'uuid' => '6eed7c9e-88e1-1e4a-b289-5c9c083a749d',
            'r_name' => 'America/Argentina/Salta',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 69,
            'uuid' => 'e3e5e78f-e751-df6c-3ca3-046222c58031',
            'r_name' => 'America/Argentina/San_Juan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 70,
            'uuid' => '5805902d-6096-256b-138f-3e888c15bd29',
            'r_name' => 'America/Argentina/San_Luis',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 71,
            'uuid' => '64dc7446-3721-710b-a998-d0f898a01153',
            'r_name' => 'America/Argentina/Tucuman',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 72,
            'uuid' => '03c246b3-51e0-908d-edae-6c909a257dab',
            'r_name' => 'America/Argentina/Ushuaia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 73,
            'uuid' => '3c027e02-f2cb-0a27-9e11-4805eba61967',
            'r_name' => 'America/Aruba',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 74,
            'uuid' => '3aa96d6c-5808-e29d-29ba-cde696db5009',
            'r_name' => 'America/Asuncion',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 75,
            'uuid' => 'e9559042-1f5b-85b3-33d5-3a789b094fb8',
            'r_name' => 'America/Atikokan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 76,
            'uuid' => 'eb0c0cf2-d0a6-5c8f-cb5e-eddeb7c7363f',
            'r_name' => 'America/Atka',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 77,
            'uuid' => 'b80201f3-a39f-8591-7e23-98c2f499d24b',
            'r_name' => 'America/Bahia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 78,
            'uuid' => '87d859ed-8416-4545-f461-ec33b3dea4a6',
            'r_name' => 'America/Bahia_Banderas',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 79,
            'uuid' => '6635e699-5543-5878-87ab-0bf71dda4ee3',
            'r_name' => 'America/Barbados',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 80,
            'uuid' => '81a50da8-9d92-7920-2d51-4ff8b7079713',
            'r_name' => 'America/Belem',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 81,
            'uuid' => '82971c4f-efcc-4433-95ac-4243e30946ad',
            'r_name' => 'America/Belize',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 82,
            'uuid' => 'f5cfc671-20eb-4e6e-44e5-c2c4a16c7b0e',
            'r_name' => 'America/Blanc-Sablon',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 83,
            'uuid' => '20be5887-a246-4ad0-6590-cdf0546a5c56',
            'r_name' => 'America/Boa_Vista',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 84,
            'uuid' => '7b98cc6f-beee-7167-5ae6-8d1b4b776f45',
            'r_name' => 'America/Bogota',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 85,
            'uuid' => '4d0da9a3-73bc-802b-972d-6f9954c77897',
            'r_name' => 'America/Boise',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 86,
            'uuid' => '838f8f2d-4e46-e0f2-b2a4-966035419094',
            'r_name' => 'America/Buenos_Aires',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 87,
            'uuid' => '7bc1725d-7c50-4c38-f050-51ef6945c74f',
            'r_name' => 'America/Cambridge_Bay',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 88,
            'uuid' => '48515e52-12ec-5caa-b7bc-998a81fd916a',
            'r_name' => 'America/Campo_Grande',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 89,
            'uuid' => 'c310aaba-78b7-341b-12eb-b68f3fa02e6d',
            'r_name' => 'America/Cancun',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 90,
            'uuid' => '73d2f876-5a9b-19a1-32cb-8c9b1aacdec4',
            'r_name' => 'America/Caracas',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 91,
            'uuid' => '59947f3b-6b2e-5017-b232-2c76ade8df89',
            'r_name' => 'America/Catamarca',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 92,
            'uuid' => '8933a1e6-0e46-23ab-a8f6-892dc7cb017d',
            'r_name' => 'America/Cayenne',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 93,
            'uuid' => '8091e51c-6a26-89bb-7097-219a2af37748',
            'r_name' => 'America/Cayman',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 94,
            'uuid' => '35582e1b-a616-dc21-e108-3f6dfef4bac0',
            'r_name' => 'America/Chicago',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 95,
            'uuid' => '161ac8dc-bc3d-6a5f-c501-b8560e60b215',
            'r_name' => 'America/Chihuahua',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 96,
            'uuid' => 'cccaf242-3da1-3e01-9040-0a25d9731d1c',
            'r_name' => 'America/Coral_Harbour',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 97,
            'uuid' => 'addc5ff1-2362-a706-26ab-e233f502297e',
            'r_name' => 'America/Cordoba',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 98,
            'uuid' => '856dbf69-019b-637c-928f-4e9b3d5838db',
            'r_name' => 'America/Costa_Rica',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 99,
            'uuid' => '6d32a306-6cf2-4fe5-38a9-ad7750a4039d',
            'r_name' => 'America/Creston',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 100,
            'uuid' => 'f708fd4b-f53a-519a-dac4-527749ad8b3f',
            'r_name' => 'America/Cuiaba',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 101,
            'uuid' => 'cd43ff05-b787-edf4-580d-006d0480a83b',
            'r_name' => 'America/Curacao',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 102,
            'uuid' => '74179065-a95a-6de7-a873-cbf58eaf0ab0',
            'r_name' => 'America/Danmarkshavn',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 103,
            'uuid' => '01e37f34-8d09-0c56-95c5-8410ca232566',
            'r_name' => 'America/Dawson',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 104,
            'uuid' => '9f70274d-859a-50ff-a95b-fdd1a026b691',
            'r_name' => 'America/Dawson_Creek',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 105,
            'uuid' => '4c8c3c2f-778f-95ef-a21e-5f5047b28f7e',
            'r_name' => 'America/Denver',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 106,
            'uuid' => 'f9957111-9b62-7211-bece-2281bd2573ac',
            'r_name' => 'America/Detroit',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 107,
            'uuid' => 'd81183ba-de74-69c1-85b3-fb7388275249',
            'r_name' => 'America/Dominica',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 108,
            'uuid' => 'f29715b0-c9f2-7862-fab7-217302174d73',
            'r_name' => 'America/Edmonton',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 109,
            'uuid' => '483e22f8-ce33-aaf1-028f-caabc9bdacfc',
            'r_name' => 'America/Eirunepe',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 110,
            'uuid' => '4e4b8540-b269-ad94-d0ed-7fd78effceac',
            'r_name' => 'America/El_Salvador',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SV',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 111,
            'uuid' => 'c22b1e7b-4f0f-544a-5b6c-ef44f9289db1',
            'r_name' => 'America/Ensenada',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 112,
            'uuid' => '75b1073a-be72-23a4-5a5f-7fb946bf6edc',
            'r_name' => 'America/Fort_Nelson',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 113,
            'uuid' => 'dd719d5f-cae3-50be-d040-6db2a3016aef',
            'r_name' => 'America/Fort_Wayne',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 114,
            'uuid' => '047c7610-c028-a812-dbfa-88c929f49cd0',
            'r_name' => 'America/Fortaleza',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 115,
            'uuid' => 'e5f0f6a1-4fb0-7535-ae34-18db0e181a5a',
            'r_name' => 'America/Glace_Bay',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 116,
            'uuid' => '9d1cb33c-cdd4-53b7-1721-bf87190d6596',
            'r_name' => 'America/Godthab',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 117,
            'uuid' => '480011a1-ea9b-af92-55fc-bcef5e41cf5f',
            'r_name' => 'America/Goose_Bay',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 118,
            'uuid' => 'c8df8531-ab0e-7ea1-5af8-c63c460af46b',
            'r_name' => 'America/Grand_Turk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 119,
            'uuid' => '80e0a63a-06f7-9fb1-f354-a6975f12466a',
            'r_name' => 'America/Grenada',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 120,
            'uuid' => '329d7eac-cf13-cb17-76fd-aeb3639fd3e3',
            'r_name' => 'America/Guadeloupe',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 121,
            'uuid' => '5632efb7-9864-5e34-49b4-8b11ea9ffdbe',
            'r_name' => 'America/Guatemala',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 122,
            'uuid' => 'ad8e0ab3-e38f-e80f-7c79-9100c43c6187',
            'r_name' => 'America/Guayaquil',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'EC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 123,
            'uuid' => '20c2ac24-b4c6-c97d-d626-96df0f3a3c45',
            'r_name' => 'America/Guyana',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 124,
            'uuid' => '4206b1d8-893b-3c0f-9b3f-f4fc812fc759',
            'r_name' => 'America/Halifax',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 125,
            'uuid' => '70498c32-17ea-bfe9-52b4-1d8161d77ac7',
            'r_name' => 'America/Havana',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 126,
            'uuid' => '66ff894c-847c-16ea-b4fc-b1bcf379e791',
            'r_name' => 'America/Hermosillo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 127,
            'uuid' => '882b244c-84ef-23f3-d326-b217625ce6e0',
            'r_name' => 'America/Indiana/Indianapolis',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 128,
            'uuid' => 'b08aa9ad-a6f1-76a4-5f9f-b674b63572fb',
            'r_name' => 'America/Indiana/Knox',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 129,
            'uuid' => 'd60f4180-a7b7-f111-b3df-8ba2362a780d',
            'r_name' => 'America/Indiana/Marengo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 130,
            'uuid' => '7e7f51a5-d9c3-3e12-2577-50c58614e071',
            'r_name' => 'America/Indiana/Petersburg',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 131,
            'uuid' => '74e24a63-6ad8-1316-6da0-cc9f551484fc',
            'r_name' => 'America/Indiana/Tell_City',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 132,
            'uuid' => '65b22c1b-4215-8041-78a3-f915187f0723',
            'r_name' => 'America/Indiana/Vevay',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 133,
            'uuid' => '4c6a3271-2524-ed26-b475-bc135a436d44',
            'r_name' => 'America/Indiana/Vincennes',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 134,
            'uuid' => '553f995b-126f-1daa-cd04-063dae19f663',
            'r_name' => 'America/Indiana/Winamac',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 135,
            'uuid' => 'fbe8ee96-5972-3bfc-71cf-e1e0af72d70a',
            'r_name' => 'America/Indianapolis',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 136,
            'uuid' => '695819ab-8ab4-37e6-10dd-69af2ca04d1a',
            'r_name' => 'America/Inuvik',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 137,
            'uuid' => 'c1fd8578-55e9-6cfb-1ff2-c1a99ff06138',
            'r_name' => 'America/Iqaluit',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 138,
            'uuid' => '6d67686d-9ec9-e2be-9a73-65d05b068714',
            'r_name' => 'America/Jamaica',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'JM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 139,
            'uuid' => '2c49afce-810c-d82e-4365-17e631df3795',
            'r_name' => 'America/Jujuy',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 140,
            'uuid' => 'b39b12fb-8809-45d2-0f98-bf4398ded84b',
            'r_name' => 'America/Juneau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 141,
            'uuid' => '3639808d-f003-ea47-9802-efa1e23986c1',
            'r_name' => 'America/Kentucky/Louisville',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 142,
            'uuid' => '0a4b907f-a991-fe7a-426b-f8f020a60c0d',
            'r_name' => 'America/Kentucky/Monticello',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 143,
            'uuid' => 'f04edffb-e6ab-83ea-9995-e06f16d23203',
            'r_name' => 'America/Knox_IN',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 144,
            'uuid' => '36ee51cd-08fb-f6c5-9d26-06161091dba9',
            'r_name' => 'America/Kralendijk',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 145,
            'uuid' => 'f06812ff-66d8-983d-46d0-8d34aadced9f',
            'r_name' => 'America/La_Paz',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 146,
            'uuid' => '278bc0dc-53ed-14db-7c29-dfefc67c2f01',
            'r_name' => 'America/Lima',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 147,
            'uuid' => '5e3a14d0-7213-cf8a-b0ce-95d350b50dd1',
            'r_name' => 'America/Los_Angeles',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 148,
            'uuid' => 'fa733ca1-c2fd-2669-7147-6a8ae7483d9d',
            'r_name' => 'America/Louisville',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 149,
            'uuid' => '7bce24e6-1409-6c5c-6d35-476b96da4360',
            'r_name' => 'America/Lower_Princes',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 150,
            'uuid' => '503e58b1-ce56-781d-d4d3-3033337dbf6e',
            'r_name' => 'America/Maceio',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 151,
            'uuid' => 'f18378f8-d1f1-8d89-e4af-04ca00fc13bd',
            'r_name' => 'America/Managua',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 152,
            'uuid' => 'b094941e-f601-e92a-ac34-dbd9a6a168f9',
            'r_name' => 'America/Manaus',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 153,
            'uuid' => 'b1af6b3b-422d-f123-3946-43ee3ba9466f',
            'r_name' => 'America/Marigot',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 154,
            'uuid' => '74882878-68c1-48b8-6598-e906d6ea2070',
            'r_name' => 'America/Martinique',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 155,
            'uuid' => '52719f1d-0bfa-2a4f-2f73-986d82f2506e',
            'r_name' => 'America/Matamoros',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 156,
            'uuid' => '529c8393-8a1c-ea77-9835-a5c0988689f6',
            'r_name' => 'America/Mazatlan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 157,
            'uuid' => '1d5d1ddb-3879-9b0b-953a-992e1c97e578',
            'r_name' => 'America/Mendoza',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 158,
            'uuid' => '519f43bf-5bde-aeb7-3353-7d2701a5526f',
            'r_name' => 'America/Menominee',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 159,
            'uuid' => '2d8df6b2-27a1-8738-747f-fdeb4422f81f',
            'r_name' => 'America/Merida',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 160,
            'uuid' => 'aad3e193-d498-17fe-0f59-5354505525fd',
            'r_name' => 'America/Metlakatla',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 161,
            'uuid' => '6682ad96-1cdc-4270-ed0a-22aa32f054e3',
            'r_name' => 'America/Mexico_City',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 162,
            'uuid' => '23f64b98-b531-6172-36c3-bb3d6bb09e67',
            'r_name' => 'America/Miquelon',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 163,
            'uuid' => '204cee39-79a7-f9c4-f2d8-3dbc4fa31805',
            'r_name' => 'America/Moncton',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 164,
            'uuid' => 'e44e4b46-0a64-8294-7bdb-f838a89b234f',
            'r_name' => 'America/Monterrey',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 165,
            'uuid' => '72d83212-9175-a92c-ff14-81d39e6b8624',
            'r_name' => 'America/Montevideo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 166,
            'uuid' => '75de11fd-56f2-4641-07b8-04c7e9ded544',
            'r_name' => 'America/Montreal',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 167,
            'uuid' => '08a88be2-f90f-6a84-d777-35069509a3e6',
            'r_name' => 'America/Montserrat',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 168,
            'uuid' => '7997c728-d389-405a-58a8-8fda713b7f66',
            'r_name' => 'America/Nassau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 169,
            'uuid' => 'df418786-f97f-3f9f-8f3b-6e514f2ace8b',
            'r_name' => 'America/New_York',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 170,
            'uuid' => '63a174d6-c774-7d72-fbea-2e1326f44521',
            'r_name' => 'America/Nipigon',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 171,
            'uuid' => 'd43e27ca-b8ed-b895-5254-9e85e3f43b85',
            'r_name' => 'America/Nome',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 172,
            'uuid' => 'bd953196-780b-4665-9990-ebcf61d5623e',
            'r_name' => 'America/Noronha',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -7200,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 173,
            'uuid' => 'a44d65da-9f57-3579-e719-78e9c61e8ea7',
            'r_name' => 'America/North_Dakota/Beulah',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 174,
            'uuid' => '4cb5957e-6818-f560-d59a-5f5926ddc0e5',
            'r_name' => 'America/North_Dakota/Center',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 175,
            'uuid' => 'faf42fc4-b3a1-11d3-6fa4-77023403ccd7',
            'r_name' => 'America/North_Dakota/New_Salem',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 176,
            'uuid' => '52410552-6dbe-ec96-8467-2858da798084',
            'r_name' => 'America/Nuuk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 177,
            'uuid' => '663bac8f-8929-9224-6847-502e33031486',
            'r_name' => 'America/Ojinaga',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 178,
            'uuid' => 'bb7825c7-4150-e69e-5a40-c4df79010ff3',
            'r_name' => 'America/Panama',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 179,
            'uuid' => '6b36f6b2-87d9-c1ef-1b2c-f70ab74451d3',
            'r_name' => 'America/Pangnirtung',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 180,
            'uuid' => '111daee5-c059-6fad-3342-4e6e763dd156',
            'r_name' => 'America/Paramaribo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 181,
            'uuid' => '3918915e-58a3-bd83-e6e5-d1bb04bc1000',
            'r_name' => 'America/Phoenix',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 182,
            'uuid' => '1473f4cf-8b2f-0c59-6903-d444f71ff028',
            'r_name' => 'America/Port-au-Prince',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'HT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 183,
            'uuid' => '0605498d-885f-7ac6-ff21-a21a9358ecdb',
            'r_name' => 'America/Port_of_Spain',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 184,
            'uuid' => '2cd218c6-1fb6-ca0f-9593-ffaf16d81f73',
            'r_name' => 'America/Porto_Acre',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 185,
            'uuid' => '99694dee-67d4-9487-72a7-b92a40f8ba38',
            'r_name' => 'America/Porto_Velho',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 186,
            'uuid' => '6498e054-c804-d12c-63b3-6d902b7e32d9',
            'r_name' => 'America/Puerto_Rico',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 187,
            'uuid' => 'ec5c00d1-cfc1-4f75-c626-ddc52e7678ee',
            'r_name' => 'America/Punta_Arenas',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 188,
            'uuid' => '779a6f02-3886-1f2e-955b-86f352857701',
            'r_name' => 'America/Rainy_River',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 189,
            'uuid' => 'b8abad05-5b54-9420-c0f1-9b519e4ee29d',
            'r_name' => 'America/Rankin_Inlet',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 190,
            'uuid' => '55f46c81-832f-31b1-bab2-c4b00208b41f',
            'r_name' => 'America/Recife',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 191,
            'uuid' => '14fab5c2-b161-2b80-cead-db59fe17006c',
            'r_name' => 'America/Regina',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 192,
            'uuid' => '2b921e0f-d201-d73a-409a-cd4207d12ad8',
            'r_name' => 'America/Resolute',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 193,
            'uuid' => '0b5648cc-451c-74ba-958c-61dc69f7b2a5',
            'r_name' => 'America/Rio_Branco',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 194,
            'uuid' => '91fe38e1-6346-bbf0-3b52-95a3e37093d4',
            'r_name' => 'America/Rosario',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 195,
            'uuid' => '9d61a4e1-5e13-282d-b369-7444a1fcc95a',
            'r_name' => 'America/Santa_Isabel',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 196,
            'uuid' => 'cb257c94-a40b-4522-6239-b9465cb11a43',
            'r_name' => 'America/Santarem',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 197,
            'uuid' => 'ea6e2692-7441-69c9-f6f6-48ff33019f67',
            'r_name' => 'America/Santiago',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 198,
            'uuid' => '10ebfb96-44da-ef41-7dce-11386a91a526',
            'r_name' => 'America/Santo_Domingo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 199,
            'uuid' => '4d5d3c5a-5031-ab95-bf6e-80cbd5d7ccfd',
            'r_name' => 'America/Sao_Paulo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 200,
            'uuid' => 'd3183bde-85a7-edb4-2cb3-2f4c5cc57e5d',
            'r_name' => 'America/Scoresbysund',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -3600,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 201,
            'uuid' => '495a5d0c-f8ed-ea04-de10-8e238d239b4f',
            'r_name' => 'America/Shiprock',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 202,
            'uuid' => 'a357491a-9669-4cc5-03af-0f4e561e6429',
            'r_name' => 'America/Sitka',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 203,
            'uuid' => 'd0dbe994-a1e9-99a9-7b25-0738e7bef19a',
            'r_name' => 'America/St_Barthelemy',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 204,
            'uuid' => '87786376-b7f8-54dc-a602-381b1ea0e765',
            'r_name' => 'America/St_Johns',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -12600,
            'dst_l10n_datetime_utc_offset_seconds' => -9000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 205,
            'uuid' => 'a7865f19-143b-3ef5-5b5e-2302b805556f',
            'r_name' => 'America/St_Kitts',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 206,
            'uuid' => 'f67583fa-97ea-29c6-c6bf-d83fa4bf647d',
            'r_name' => 'America/St_Lucia',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 207,
            'uuid' => '56805a7f-1f79-4fc5-05ba-2e909ba23a2f',
            'r_name' => 'America/St_Thomas',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 208,
            'uuid' => '089c6c1d-2b7e-ede2-78c9-446e1303d672',
            'r_name' => 'America/St_Vincent',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 209,
            'uuid' => 'cef70098-63fd-7959-7ad7-60e6e22c0ce3',
            'r_name' => 'America/Swift_Current',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 210,
            'uuid' => '2e28a45a-0bc4-0814-18e7-0fc7b309101c',
            'r_name' => 'America/Tegucigalpa',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'HN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 211,
            'uuid' => '8eed7e9b-2e30-a3b3-ba9a-9a1d8c51cc21',
            'r_name' => 'America/Thule',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 212,
            'uuid' => '9714c496-dba6-1ab4-4737-e895402a8b50',
            'r_name' => 'America/Thunder_Bay',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 213,
            'uuid' => 'ffdb299b-2aab-e975-4bae-ef10627d3234',
            'r_name' => 'America/Tijuana',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 214,
            'uuid' => '46f6b884-babd-9889-b577-7e21311e0ed2',
            'r_name' => 'America/Toronto',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 215,
            'uuid' => '84f17c99-a37d-7771-9af9-0c17066e02da',
            'r_name' => 'America/Tortola',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 216,
            'uuid' => '0ba15eab-28f3-d969-0914-57218c61611a',
            'r_name' => 'America/Vancouver',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 217,
            'uuid' => '82e4d08d-03d5-1ec7-eac1-d6310b74d59e',
            'r_name' => 'America/Virgin',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 218,
            'uuid' => '611fb18a-edfb-9cfe-dda5-f9e83394d1ed',
            'r_name' => 'America/Whitehorse',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 219,
            'uuid' => '001833c1-bc1e-0cee-73f5-3e02f3fabcd0',
            'r_name' => 'America/Winnipeg',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 220,
            'uuid' => 'c127e33d-9011-9633-484a-7796b888abfc',
            'r_name' => 'America/Yakutat',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 221,
            'uuid' => '9aa004de-c26f-296e-b2b9-94d4ca88e9e2',
            'r_name' => 'America/Yellowknife',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 222,
            'uuid' => 'ded1737f-1425-f765-0aa5-b2a8bc3f1b98',
            'r_name' => 'Antarctica/Casey',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 223,
            'uuid' => '691cbac8-ee78-92bc-d85e-e71f817d97f7',
            'r_name' => 'Antarctica/Davis',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 224,
            'uuid' => '2b98820f-d52d-27d1-6def-4083defaa7f8',
            'r_name' => 'Antarctica/DumontDUrville',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 225,
            'uuid' => 'db77e870-a9a8-e5c4-779a-2338ed061dae',
            'r_name' => 'Antarctica/Macquarie',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 226,
            'uuid' => '0ad9b470-1522-fd78-f1ce-7960403cf664',
            'r_name' => 'Antarctica/Mawson',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 227,
            'uuid' => '7bc76a1e-f23b-ef86-098a-d9ee78cfd9c7',
            'r_name' => 'Antarctica/McMurdo',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 228,
            'uuid' => 'f030ce7c-dab7-a85c-dd54-a5bf11bf2b7a',
            'r_name' => 'Antarctica/Palmer',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 229,
            'uuid' => 'a3b33b69-485d-2376-3a7a-06f59d7f15f0',
            'r_name' => 'Antarctica/Rothera',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 230,
            'uuid' => 'd65e2c40-aed0-0756-2361-812e3516f6e9',
            'r_name' => 'Antarctica/South_Pole',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 231,
            'uuid' => 'de0edb7a-a624-71fe-964c-fdd92f56bd1a',
            'r_name' => 'Antarctica/Syowa',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 232,
            'uuid' => '23db308e-0489-d48f-cbe7-7dfc22d26947',
            'r_name' => 'Antarctica/Troll',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 233,
            'uuid' => 'cfee560e-591e-afaf-0dec-d146c601a426',
            'r_name' => 'Antarctica/Vostok',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 234,
            'uuid' => '85023a3d-ee64-fc10-711d-d21ed4a16e4b',
            'r_name' => 'Arctic/Longyearbyen',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SJ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 235,
            'uuid' => '5dc71b8b-23c5-02ba-9c39-04abebe439a3',
            'r_name' => 'Asia/Aden',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'YE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 236,
            'uuid' => '56a8aed3-03c4-8fb8-3fcb-db1a22f2375c',
            'r_name' => 'Asia/Almaty',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 237,
            'uuid' => 'fbb14bf8-f76a-e307-382f-d8ef3c3a114e',
            'r_name' => 'Asia/Amman',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'JO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 238,
            'uuid' => 'df6be40d-2bda-3a57-8b8e-5e92e1beafe7',
            'r_name' => 'Asia/Anadyr',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 239,
            'uuid' => '6ba65ec4-cb2a-1a77-3e3c-c3c25965ce3c',
            'r_name' => 'Asia/Aqtau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 240,
            'uuid' => '630277c7-6f8d-0f07-c956-943ee3c78c9a',
            'r_name' => 'Asia/Aqtobe',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 241,
            'uuid' => 'ac4fba65-4d45-cc18-a020-21a9ba424b2f',
            'r_name' => 'Asia/Ashgabat',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 242,
            'uuid' => '53f96f64-0eca-93f9-8fcd-57894bdc02d0',
            'r_name' => 'Asia/Ashkhabad',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 243,
            'uuid' => 'c97414a1-60bd-e487-084a-78ba74003bbf',
            'r_name' => 'Asia/Atyrau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 244,
            'uuid' => 'b56df091-1aba-ac07-3710-b954f7349977',
            'r_name' => 'Asia/Baghdad',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IQ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 245,
            'uuid' => 'cfb40d79-1a86-fe70-9568-bdfa223692d5',
            'r_name' => 'Asia/Bahrain',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 246,
            'uuid' => 'dc0d702c-a429-d8ab-ea8d-4b84e796c7fd',
            'r_name' => 'Asia/Baku',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 247,
            'uuid' => 'd406b20b-cc4f-1bfd-ef4c-d7dc2b9d9779',
            'r_name' => 'Asia/Bangkok',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 248,
            'uuid' => '29adf4f3-f68b-b5fc-277a-5a0f42602696',
            'r_name' => 'Asia/Barnaul',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 249,
            'uuid' => '2a90c12c-4688-6522-8a41-ed94d613487d',
            'r_name' => 'Asia/Beirut',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 250,
            'uuid' => '7200ad6b-2e4c-56b1-50ce-07f1fb7b5943',
            'r_name' => 'Asia/Bishkek',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 251,
            'uuid' => '2418d23b-2e52-256b-bde4-95a2630138b0',
            'r_name' => 'Asia/Brunei',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 252,
            'uuid' => 'd00ea417-ab7a-699d-c08b-1ee813f8efd9',
            'r_name' => 'Asia/Calcutta',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 19800,
            'dst_l10n_datetime_utc_offset_seconds' => 19800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 253,
            'uuid' => '861f7c47-0241-0275-8cb1-422246cfd0c2',
            'r_name' => 'Asia/Chita',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 254,
            'uuid' => '556a3ee5-8582-16bb-2333-6e10400e75fb',
            'r_name' => 'Asia/Choibalsan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 255,
            'uuid' => '3965c4e5-22f4-2462-7145-5765a4e10d04',
            'r_name' => 'Asia/Chongqing',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 256,
            'uuid' => '31d6f1e9-a9ff-ae91-6e10-6147da720b27',
            'r_name' => 'Asia/Chungking',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 257,
            'uuid' => '7e588e18-c37c-fc09-05e3-7aa4faec8fe1',
            'r_name' => 'Asia/Colombo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 19800,
            'dst_l10n_datetime_utc_offset_seconds' => 19800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 258,
            'uuid' => '99a878f5-2747-3d79-a410-feb0e25bff6a',
            'r_name' => 'Asia/Dacca',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 259,
            'uuid' => '9e47dc42-f489-ab21-953c-64a557df2254',
            'r_name' => 'Asia/Damascus',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 260,
            'uuid' => '8b7f019d-29e1-58a0-833e-208dc8824757',
            'r_name' => 'Asia/Dhaka',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 261,
            'uuid' => '39f3d1c7-7fbd-4e78-b66b-26dc1203fc5e',
            'r_name' => 'Asia/Dili',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 262,
            'uuid' => '98b00403-19f1-ad55-2c49-186b6ad82f22',
            'r_name' => 'Asia/Dubai',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 263,
            'uuid' => 'be44110d-548d-da30-fbda-330e35538215',
            'r_name' => 'Asia/Dushanbe',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TJ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 264,
            'uuid' => '67d9c5bb-df94-002a-192d-695c9b63255e',
            'r_name' => 'Asia/Famagusta',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 265,
            'uuid' => 'b58ea004-7d38-d660-cec5-b8a9e953413d',
            'r_name' => 'Asia/Gaza',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 266,
            'uuid' => 'a3de1174-8694-0ba5-a8f0-f56d60180afd',
            'r_name' => 'Asia/Harbin',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 267,
            'uuid' => '988df4c1-4c3a-f262-5636-a82b47b21f88',
            'r_name' => 'Asia/Hebron',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 268,
            'uuid' => 'f8653743-90d1-f6fe-c167-5bfd1dc249bc',
            'r_name' => 'Asia/Ho_Chi_Minh',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 269,
            'uuid' => 'de7ba9cf-385a-bc54-17a8-af6693fbdbb1',
            'r_name' => 'Asia/Hong_Kong',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'HK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 270,
            'uuid' => '9659f333-5a45-d7d0-d670-1b223ceecd8e',
            'r_name' => 'Asia/Hovd',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 271,
            'uuid' => '1805ab0e-4a50-9dce-ea78-4dd36abf9ce1',
            'r_name' => 'Asia/Irkutsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 272,
            'uuid' => '813e2125-d378-3df8-ad91-dedbee21325f',
            'r_name' => 'Asia/Istanbul',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 273,
            'uuid' => 'dab3dff1-8af6-ad8f-5096-5c3c11902676',
            'r_name' => 'Asia/Jakarta',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ID',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 274,
            'uuid' => '96e16678-e3c6-c905-ebfb-e8cb15245d50',
            'r_name' => 'Asia/Jayapura',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ID',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 275,
            'uuid' => 'd354ecea-e1eb-d69b-5d90-bdb77c7e85ba',
            'r_name' => 'Asia/Jerusalem',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 276,
            'uuid' => '224d550d-ffce-58a5-dcce-45cade81c184',
            'r_name' => 'Asia/Kabul',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 16200,
            'dst_l10n_datetime_utc_offset_seconds' => 16200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 277,
            'uuid' => '01e57d1c-bdb3-15a0-b841-9ca892b66156',
            'r_name' => 'Asia/Kamchatka',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 278,
            'uuid' => '2090ad19-5891-c33a-6af0-c24559b43cbe',
            'r_name' => 'Asia/Karachi',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 279,
            'uuid' => '11940053-82a5-8fef-0dca-a34bba81594e',
            'r_name' => 'Asia/Kashgar',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 280,
            'uuid' => '072415d1-8c54-88b7-d1fe-e65125e5b820',
            'r_name' => 'Asia/Kathmandu',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 20700,
            'dst_l10n_datetime_utc_offset_seconds' => 20700,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 281,
            'uuid' => '51e1310b-607e-7714-a088-582f2a881c82',
            'r_name' => 'Asia/Katmandu',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 20700,
            'dst_l10n_datetime_utc_offset_seconds' => 20700,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 282,
            'uuid' => 'e196085d-a9fc-5213-14d8-72c457b9cd4c',
            'r_name' => 'Asia/Khandyga',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 283,
            'uuid' => '7d8593fe-11dd-6dda-5d96-df2c3ec2ff12',
            'r_name' => 'Asia/Kolkata',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 19800,
            'dst_l10n_datetime_utc_offset_seconds' => 19800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 284,
            'uuid' => '560dce71-345a-b752-b35b-684791465cca',
            'r_name' => 'Asia/Krasnoyarsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 285,
            'uuid' => '866b846d-acc0-b06f-c18f-35100e7d720c',
            'r_name' => 'Asia/Kuala_Lumpur',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 286,
            'uuid' => '509a88cb-96e0-0e09-3536-a4389fa35710',
            'r_name' => 'Asia/Kuching',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 287,
            'uuid' => 'b2a2acc7-fcb6-4d27-08e5-87629125e20b',
            'r_name' => 'Asia/Kuwait',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 288,
            'uuid' => '1286140b-9390-c37e-859b-bd4d15ee18ff',
            'r_name' => 'Asia/Macao',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 289,
            'uuid' => '580b153b-d36b-fac6-ffb5-dbd3f15cd2cc',
            'r_name' => 'Asia/Macau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 290,
            'uuid' => '3cc9a8cd-4890-57f7-764d-13486f176fc7',
            'r_name' => 'Asia/Magadan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 291,
            'uuid' => 'b1d721b9-10a4-e3fd-cc3e-7ae4bae1c54e',
            'r_name' => 'Asia/Makassar',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ID',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 292,
            'uuid' => '06219b9d-4de8-3a17-b636-9020ed04ede3',
            'r_name' => 'Asia/Manila',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 293,
            'uuid' => 'cdc47b97-c4c3-aefb-3e95-b962c9653a7b',
            'r_name' => 'Asia/Muscat',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'OM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 294,
            'uuid' => 'a954829f-e64b-779c-98fa-7a535cd6f823',
            'r_name' => 'Asia/Nicosia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 295,
            'uuid' => '184db663-97d1-dcf6-93e8-90538c984f16',
            'r_name' => 'Asia/Novokuznetsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 296,
            'uuid' => 'da977b1d-0a00-6cc9-a403-f175543281ef',
            'r_name' => 'Asia/Novosibirsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 297,
            'uuid' => '2426e7d6-6f34-e322-7b68-f4c86105a42a',
            'r_name' => 'Asia/Omsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 298,
            'uuid' => '60e24236-6a92-68d0-4d3a-45b695711059',
            'r_name' => 'Asia/Oral',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 299,
            'uuid' => 'c0c8c42f-95c9-5c3a-05de-a156944d9ad0',
            'r_name' => 'Asia/Phnom_Penh',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 300,
            'uuid' => '0914bff8-1706-ad2c-9a66-0f93267092a3',
            'r_name' => 'Asia/Pontianak',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ID',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 301,
            'uuid' => '7a7bb24d-4850-e29f-b50f-a465897b5fa7',
            'r_name' => 'Asia/Pyongyang',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 302,
            'uuid' => 'f11b52d9-581a-7182-e046-6fb379b2603a',
            'r_name' => 'Asia/Qatar',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'QA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 303,
            'uuid' => '0da4f574-87df-85eb-0246-19aa33c15ae2',
            'r_name' => 'Asia/Qostanay',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 304,
            'uuid' => 'bde0e093-fe58-df35-0335-b449fdc057f1',
            'r_name' => 'Asia/Qyzylorda',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 305,
            'uuid' => '90cf0334-d1ce-63d2-9f36-5ae7813bfca9',
            'r_name' => 'Asia/Rangoon',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 23400,
            'dst_l10n_datetime_utc_offset_seconds' => 23400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 306,
            'uuid' => 'ebc2ce2c-2250-48d4-ea49-8c169316f47c',
            'r_name' => 'Asia/Riyadh',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 307,
            'uuid' => '39b44f2a-99b6-708a-ab91-9d5b7417c9e1',
            'r_name' => 'Asia/Saigon',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 308,
            'uuid' => '9752887f-1e44-fa34-63a4-2a6bfd0e3702',
            'r_name' => 'Asia/Sakhalin',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 309,
            'uuid' => 'ffd72186-c764-1b64-ee22-bd3742b8c09b',
            'r_name' => 'Asia/Samarkand',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 310,
            'uuid' => '5b10928f-3ad9-6ad0-dacd-4cfa3662ea0c',
            'r_name' => 'Asia/Seoul',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 311,
            'uuid' => 'cf74cd76-5a57-a0cf-5b63-9820299a0885',
            'r_name' => 'Asia/Shanghai',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 312,
            'uuid' => 'd755b188-ced8-4818-1d6e-9331105f206f',
            'r_name' => 'Asia/Singapore',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 313,
            'uuid' => '8df15165-cefa-571e-ba1a-81fd5e3d29ec',
            'r_name' => 'Asia/Srednekolymsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 314,
            'uuid' => 'bd04cba4-88e3-dc4a-3c69-53fbe7bd75cf',
            'r_name' => 'Asia/Taipei',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 315,
            'uuid' => 'fd5f6ef5-25f5-a957-5a71-dc284d37f001',
            'r_name' => 'Asia/Tashkent',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 316,
            'uuid' => 'e41f82c5-5636-9e0c-f0a8-939ece11d99b',
            'r_name' => 'Asia/Tbilisi',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 317,
            'uuid' => '4186e916-6cfb-538d-beef-b85a3988a4ae',
            'r_name' => 'Asia/Tehran',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 12600,
            'dst_l10n_datetime_utc_offset_seconds' => 16200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 318,
            'uuid' => '2c532b87-0f95-ca33-1121-f99d6088a6ae',
            'r_name' => 'Asia/Tel_Aviv',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 319,
            'uuid' => '40fe821f-0526-1146-7c49-62fac7c66820',
            'r_name' => 'Asia/Thimbu',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 320,
            'uuid' => '039b9bbe-ac24-8ae7-2100-817b01d22ecc',
            'r_name' => 'Asia/Thimphu',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 321,
            'uuid' => 'a525fdf4-db88-fede-92dd-964d78fd3a71',
            'r_name' => 'Asia/Tokyo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'JP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 322,
            'uuid' => '54931706-21b8-58f6-ebb2-141da8283ba9',
            'r_name' => 'Asia/Tomsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 323,
            'uuid' => 'f6b84ea0-3fcc-88db-6a56-c5890844d8c5',
            'r_name' => 'Asia/Ujung_Pandang',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ID',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 324,
            'uuid' => 'f27f92ec-e7c3-0e1b-d2a6-0303f1233a36',
            'r_name' => 'Asia/Ulaanbaatar',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 325,
            'uuid' => 'ff75b979-d5b3-fbe8-b4d8-dff5242e9737',
            'r_name' => 'Asia/Ulan_Bator',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 326,
            'uuid' => '6a79e7ec-5a48-4f47-a0b5-3eb4948817ae',
            'r_name' => 'Asia/Urumqi',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 327,
            'uuid' => 'be1101bf-1e94-f05c-3232-5e93d5fdd9a0',
            'r_name' => 'Asia/Ust-Nera',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 328,
            'uuid' => 'd45ecf56-d896-4d33-2cea-b0e4de8461cf',
            'r_name' => 'Asia/Vientiane',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 329,
            'uuid' => 'c78362a8-60bc-28ea-58b8-af9d0ce48000',
            'r_name' => 'Asia/Vladivostok',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 330,
            'uuid' => 'f58bdc60-b498-fbd0-c791-6dda9fc8933a',
            'r_name' => 'Asia/Yakutsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 331,
            'uuid' => 'de55bd81-0aa2-d17c-f5d9-93612b72fbd7',
            'r_name' => 'Asia/Yangon',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 23400,
            'dst_l10n_datetime_utc_offset_seconds' => 23400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 332,
            'uuid' => '50a7a9c8-63a5-505f-d8c4-05fcf0385f9a',
            'r_name' => 'Asia/Yekaterinburg',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 333,
            'uuid' => '28bd9b6b-c63a-3999-4e73-3f538157b335',
            'r_name' => 'Asia/Yerevan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 334,
            'uuid' => '0e81a210-2c8c-513b-69e7-4c05f2bec0b3',
            'r_name' => 'Atlantic/Azores',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -3600,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 335,
            'uuid' => 'e2a09649-b19b-2065-618f-00bca9e81bd1',
            'r_name' => 'Atlantic/Bermuda',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 336,
            'uuid' => '88d3fd41-1a89-ad41-3ac8-b9e19be07cd6',
            'r_name' => 'Atlantic/Canary',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ES',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 337,
            'uuid' => '3ad99322-9b3a-3c94-789e-808ffc2810fa',
            'r_name' => 'Atlantic/Cape_Verde',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -3600,
            'dst_l10n_datetime_utc_offset_seconds' => -3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CV',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 338,
            'uuid' => 'd6d852a3-95ee-6621-6797-df359514851c',
            'r_name' => 'Atlantic/Faeroe',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 339,
            'uuid' => 'bba8cb5e-5ad2-4e58-9f26-b2179d1a6654',
            'r_name' => 'Atlantic/Faroe',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 340,
            'uuid' => '60108d50-0eac-641d-b57d-9ff97506bf8c',
            'r_name' => 'Atlantic/Jan_Mayen',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SJ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 341,
            'uuid' => '35fa0221-41fc-a6cd-8abe-157d5857e0f7',
            'r_name' => 'Atlantic/Madeira',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 342,
            'uuid' => '4cd91c8b-0b05-4606-b385-ae43741a2b49',
            'r_name' => 'Atlantic/Reykjavik',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 343,
            'uuid' => 'd7be9318-b984-d900-d084-e5941c8e6603',
            'r_name' => 'Atlantic/South_Georgia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -7200,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 344,
            'uuid' => 'aca5af21-f1fb-8476-f99e-9d59deebe38e',
            'r_name' => 'Atlantic/St_Helena',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 345,
            'uuid' => '6cb4cc19-079c-f47b-468c-aabb04f29e06',
            'r_name' => 'Atlantic/Stanley',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 346,
            'uuid' => '9ba51266-43fb-b0e5-a952-e91a8922b23c',
            'r_name' => 'Australia/ACT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 347,
            'uuid' => 'bd4516d8-cb35-34fa-280c-46bb88a45b1f',
            'r_name' => 'Australia/Adelaide',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 34200,
            'dst_l10n_datetime_utc_offset_seconds' => 37800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 348,
            'uuid' => '53b591a5-be5c-6655-d417-eae3d6764225',
            'r_name' => 'Australia/Brisbane',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 349,
            'uuid' => '1cb2255f-7ce3-b8e8-b93f-0470293a75d1',
            'r_name' => 'Australia/Broken_Hill',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 34200,
            'dst_l10n_datetime_utc_offset_seconds' => 37800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 350,
            'uuid' => '60e88b50-f3d8-100e-a90f-94f5587ccc7a',
            'r_name' => 'Australia/Canberra',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 351,
            'uuid' => 'a5dc9d3a-0a2a-e96e-0d24-dc007cb100bb',
            'r_name' => 'Australia/Currie',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 352,
            'uuid' => '3316a22c-e1bc-fd4e-1f9b-820704fcebcd',
            'r_name' => 'Australia/Darwin',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 34200,
            'dst_l10n_datetime_utc_offset_seconds' => 34200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 353,
            'uuid' => 'a7f12001-632f-2f6d-a910-4edb9bfb578b',
            'r_name' => 'Australia/Eucla',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 29700,
            'dst_l10n_datetime_utc_offset_seconds' => 29700,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 354,
            'uuid' => '2839273f-e59d-a9e8-ffc9-e7bf0b1c5f11',
            'r_name' => 'Australia/Hobart',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 355,
            'uuid' => 'c18d40bc-6250-e6f9-e827-6d2c76f38c0a',
            'r_name' => 'Australia/LHI',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 37800,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 356,
            'uuid' => '5a854d17-b6e6-99d5-5469-a446066972e6',
            'r_name' => 'Australia/Lindeman',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 357,
            'uuid' => '70dd204e-5fda-daf4-dae3-12b684690818',
            'r_name' => 'Australia/Lord_Howe',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 37800,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 358,
            'uuid' => 'bfbdf367-efcb-8f17-526f-86e32b8fad61',
            'r_name' => 'Australia/Melbourne',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 359,
            'uuid' => '69969a53-f382-982c-2728-8e69b67af3a5',
            'r_name' => 'Australia/North',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 34200,
            'dst_l10n_datetime_utc_offset_seconds' => 34200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 360,
            'uuid' => 'b39ddd6d-cdf2-049c-c4eb-2a4c2d4ae0c0',
            'r_name' => 'Australia/NSW',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 361,
            'uuid' => '942b66d3-3226-9c56-148c-e33aebc4841a',
            'r_name' => 'Australia/Perth',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 362,
            'uuid' => '7a37cbcb-8073-c059-da70-363e2068dfa4',
            'r_name' => 'Australia/Queensland',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 363,
            'uuid' => 'b3b8da32-33cd-1cec-a9fc-adf43ea901a2',
            'r_name' => 'Australia/South',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 34200,
            'dst_l10n_datetime_utc_offset_seconds' => 37800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 364,
            'uuid' => 'd79bb00b-84fa-a239-7aee-6894770e4dda',
            'r_name' => 'Australia/Sydney',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 365,
            'uuid' => 'fdd6c7d2-e104-0e32-18d0-19f8a842e97c',
            'r_name' => 'Australia/Tasmania',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 366,
            'uuid' => '5836c246-2b4c-42f4-a52c-ca058ecab68c',
            'r_name' => 'Australia/Victoria',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 367,
            'uuid' => '43149191-2ee9-4a11-7c47-12a7873b8766',
            'r_name' => 'Australia/West',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 368,
            'uuid' => 'f9433e34-34c4-0e3b-2988-c952f7e3db93',
            'r_name' => 'Australia/Yancowinna',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 34200,
            'dst_l10n_datetime_utc_offset_seconds' => 37800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 369,
            'uuid' => 'c38e91ab-a281-8afa-9e32-890e60c9c35c',
            'r_name' => 'Brazil/Acre',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 370,
            'uuid' => 'd57ad5fb-9a9f-390a-032d-123370662ad2',
            'r_name' => 'Brazil/DeNoronha',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -7200,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 371,
            'uuid' => '6844c0af-5cc7-cfc9-d6ee-e72a95464e80',
            'r_name' => 'Brazil/East',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 372,
            'uuid' => '509c4023-5962-4a16-8720-07ec153245bd',
            'r_name' => 'Brazil/West',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 373,
            'uuid' => '41973bfc-0acb-cf0d-b3c4-dbc46079ef0b',
            'r_name' => 'Canada/Atlantic',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 374,
            'uuid' => 'd54082d4-fc68-10c5-f0e1-dfede3b8fc9f',
            'r_name' => 'Canada/Central',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 375,
            'uuid' => 'ac844a94-d855-cb51-d890-2e2ef3b393d9',
            'r_name' => 'Canada/Eastern',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 376,
            'uuid' => '0477dd9d-6e5e-87cf-e475-0da83257b222',
            'r_name' => 'Canada/Mountain',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 377,
            'uuid' => '27f7684a-544c-b319-b575-4a141996fbd3',
            'r_name' => 'Canada/Newfoundland',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -12600,
            'dst_l10n_datetime_utc_offset_seconds' => -9000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 378,
            'uuid' => 'a22ce989-35b1-bb73-df33-c3d700f3e7e9',
            'r_name' => 'Canada/Pacific',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 379,
            'uuid' => '0af193d4-7a34-efe7-93b2-c4d37bc31349',
            'r_name' => 'Canada/Saskatchewan',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 380,
            'uuid' => '33dab036-18e3-611d-5f3f-78dfe1a3f51b',
            'r_name' => 'Canada/Yukon',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 381,
            'uuid' => 'b0a61f03-0076-5fc4-8759-623906172618',
            'r_name' => 'CET',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 382,
            'uuid' => '093c9399-fc0c-c1fa-3c2d-a95536205b22',
            'r_name' => 'Chile/Continental',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 383,
            'uuid' => '43bf0275-5b26-1887-75f3-0c7e13aeb19c',
            'r_name' => 'Chile/EasterIsland',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 384,
            'uuid' => '8895f110-acbd-1451-a7b1-4443c7cfc7df',
            'r_name' => 'CST6CDT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 385,
            'uuid' => '108e450f-2330-e3a0-462e-f630ea296757',
            'r_name' => 'Cuba',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 386,
            'uuid' => 'bdde7f7e-f1cd-1449-0374-6afe2f2fe631',
            'r_name' => 'EET',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 387,
            'uuid' => '7943aae4-2ce0-27a1-8a19-86e3d760e387',
            'r_name' => 'Egypt',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'EG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 388,
            'uuid' => 'a7cb3ff9-1144-54aa-87e5-630f6e024e53',
            'r_name' => 'Eire',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 389,
            'uuid' => '865480e6-4274-78c8-e7e3-5dd0a0a23390',
            'r_name' => 'EST',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 390,
            'uuid' => 'cce040d1-b7d6-13ec-027d-d671de513cac',
            'r_name' => 'EST5EDT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 391,
            'uuid' => '826b9569-fe91-18fd-fde0-69f34e7a8e93',
            'r_name' => 'Etc/GMT',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 392,
            'uuid' => '152807d6-080c-759e-6552-f119ee949f8c',
            'r_name' => 'Etc/GMT+0',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 393,
            'uuid' => '1f50bb80-95c9-2771-04ca-e644d66ac132',
            'r_name' => 'Etc/GMT+1',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -3600,
            'dst_l10n_datetime_utc_offset_seconds' => -3600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 394,
            'uuid' => 'dd26b9cf-3a48-6b86-7048-868214d9df3f',
            'r_name' => 'Etc/GMT+10',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 395,
            'uuid' => '225cba55-29e4-f598-e4f2-de450773f6ba',
            'r_name' => 'Etc/GMT+11',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -39600,
            'dst_l10n_datetime_utc_offset_seconds' => -39600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 396,
            'uuid' => '32a9864e-c0f1-1a22-a99c-6e69ae331e2a',
            'r_name' => 'Etc/GMT+12',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -43200,
            'dst_l10n_datetime_utc_offset_seconds' => -43200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 397,
            'uuid' => 'c741e198-c613-0591-8a15-972bda7d2212',
            'r_name' => 'Etc/GMT+2',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -7200,
            'dst_l10n_datetime_utc_offset_seconds' => -7200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 398,
            'uuid' => 'd669d99b-1f78-cc83-0492-f612f5fb180b',
            'r_name' => 'Etc/GMT+3',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -10800,
            'dst_l10n_datetime_utc_offset_seconds' => -10800,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 399,
            'uuid' => '700dfa0d-f3e9-bd6b-277d-653474deaa77',
            'r_name' => 'Etc/GMT+4',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -14400,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 400,
            'uuid' => '7a346183-d695-b1e3-9859-10e927ca9e85',
            'r_name' => 'Etc/GMT+5',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 401,
            'uuid' => '17538c3f-99df-9616-de63-977a874a51ba',
            'r_name' => 'Etc/GMT+6',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 402,
            'uuid' => 'bf10335a-cf7c-2edb-f186-fecaeb692e70',
            'r_name' => 'Etc/GMT+7',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 403,
            'uuid' => 'dccbd29b-d81a-c84c-f01c-656a5f265466',
            'r_name' => 'Etc/GMT+8',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 404,
            'uuid' => 'bb60aaf8-7435-3a94-2f86-c1295061abcb',
            'r_name' => 'Etc/GMT+9',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -32400,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 405,
            'uuid' => '74c0077c-b51a-654d-b3d4-daea01eefd7f',
            'r_name' => 'Etc/GMT-0',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 406,
            'uuid' => 'b09996cb-1e50-7751-3798-dc17334e00bb',
            'r_name' => 'Etc/GMT-1',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 407,
            'uuid' => '3771b93b-272c-9b72-9a0d-986c9ccaa461',
            'r_name' => 'Etc/GMT-10',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 408,
            'uuid' => '53306958-dd28-3d2f-2c07-b78930530ac5',
            'r_name' => 'Etc/GMT-11',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 409,
            'uuid' => '2053728a-6bb3-76e0-b13c-a6b5ae60caff',
            'r_name' => 'Etc/GMT-12',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 410,
            'uuid' => 'ecae64ee-c2c6-d05d-db80-9075e09b8c79',
            'r_name' => 'Etc/GMT-13',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 46800,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 411,
            'uuid' => '0ac77071-8171-b297-391b-0b184e534b08',
            'r_name' => 'Etc/GMT-14',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 50400,
            'dst_l10n_datetime_utc_offset_seconds' => 50400,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 412,
            'uuid' => '8fc30cc7-0754-3eb6-f47f-e4b54701a759',
            'r_name' => 'Etc/GMT-2',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 413,
            'uuid' => '2a3bf102-aeaa-26a7-a2e1-efff41dd5cb5',
            'r_name' => 'Etc/GMT-3',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 414,
            'uuid' => '6c11b86e-e459-f04e-32c8-3f3c4aac2906',
            'r_name' => 'Etc/GMT-4',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 415,
            'uuid' => '5053dc02-3827-b894-22e9-ffd78ec6d85c',
            'r_name' => 'Etc/GMT-5',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 416,
            'uuid' => 'b1060c41-c122-7c5e-f386-82612b89a67b',
            'r_name' => 'Etc/GMT-6',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 417,
            'uuid' => '27a0b153-c9e4-b44f-8232-04a6fb2546b5',
            'r_name' => 'Etc/GMT-7',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 418,
            'uuid' => '036301ae-df63-ef4e-42a3-fba4cf59cd50',
            'r_name' => 'Etc/GMT-8',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 419,
            'uuid' => '10819d15-5956-256b-4748-79459b44b10e',
            'r_name' => 'Etc/GMT-9',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 420,
            'uuid' => '3583aeee-d301-1fc2-b68c-21feb8ca17dd',
            'r_name' => 'Etc/GMT0',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 421,
            'uuid' => '38c86754-f3fd-4a4b-4f5f-d1a22706065c',
            'r_name' => 'Etc/Greenwich',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 422,
            'uuid' => '82bc950e-44f6-9f99-1121-67343b21967b',
            'r_name' => 'Etc/UCT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 423,
            'uuid' => 'c64ae820-dd2b-517c-0b58-9f1d65d7f05d',
            'r_name' => 'Etc/Universal',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 424,
            'uuid' => '5a38acd3-b359-7deb-4c2c-77e2073f3dce',
            'r_name' => 'Etc/UTC',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 425,
            'uuid' => '9bdc2a63-8487-b638-f44b-9b42fc3fe8eb',
            'r_name' => 'Etc/Zulu',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 426,
            'uuid' => '62c485ac-2cd3-103a-770c-e72a58b966b2',
            'r_name' => 'Europe/Amsterdam',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 427,
            'uuid' => 'f7856f76-5930-8129-cb53-aa1eb7c55e79',
            'r_name' => 'Europe/Andorra',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 428,
            'uuid' => '31305103-b42d-3df1-d977-a19e56d78668',
            'r_name' => 'Europe/Astrakhan',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 429,
            'uuid' => 'cd46f30e-a36f-d774-881b-e93005e9350a',
            'r_name' => 'Europe/Athens',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 430,
            'uuid' => '50a8ffc1-58cb-3373-9407-1ae9e9c5b67e',
            'r_name' => 'Europe/Belfast',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 431,
            'uuid' => '54f2db2b-c8c5-0bcd-553b-8a7a2ac2be61',
            'r_name' => 'Europe/Belgrade',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 432,
            'uuid' => 'a94c98c7-b9ba-8918-66df-f2a281d8c44d',
            'r_name' => 'Europe/Berlin',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 433,
            'uuid' => '5a77ad9c-8791-74d1-6ff2-85749ff9016b',
            'r_name' => 'Europe/Bratislava',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 434,
            'uuid' => '9a0effae-f679-f3c7-3485-f34708500f9c',
            'r_name' => 'Europe/Brussels',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 435,
            'uuid' => '51f68e7d-1b0d-e753-4417-ba663962be76',
            'r_name' => 'Europe/Bucharest',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 436,
            'uuid' => 'b6bac6d3-f0a3-3028-9f1c-a2f5641cb2f0',
            'r_name' => 'Europe/Budapest',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'HU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 437,
            'uuid' => '9ba8cc7e-a14f-74af-4e2a-097292274ce3',
            'r_name' => 'Europe/Busingen',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 438,
            'uuid' => '8ec6b96b-57ec-4f47-01c7-e0ba6606a8d0',
            'r_name' => 'Europe/Chisinau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 439,
            'uuid' => '1451461a-e586-d61c-cd83-0cff9cf21013',
            'r_name' => 'Europe/Copenhagen',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'DK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 440,
            'uuid' => '053805ce-69f2-d51a-71fb-087d1a0b5391',
            'r_name' => 'Europe/Dublin',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 441,
            'uuid' => 'd3705bb1-8718-413e-1bfc-a94fa351bd49',
            'r_name' => 'Europe/Gibraltar',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 442,
            'uuid' => 'eed5fe8d-cd03-cf09-cda8-428476a03f04',
            'r_name' => 'Europe/Guernsey',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 443,
            'uuid' => '7391cb3b-cc3a-9f9f-2613-2a222d6cebd7',
            'r_name' => 'Europe/Helsinki',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 444,
            'uuid' => '6fdc2689-d718-6370-a6ce-0833117d7446',
            'r_name' => 'Europe/Isle_of_Man',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 445,
            'uuid' => '62db8899-cc9e-9723-7d8d-9896d355055d',
            'r_name' => 'Europe/Istanbul',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 446,
            'uuid' => '4afb8a8d-11c7-55b2-8b50-28bba1d652c9',
            'r_name' => 'Europe/Jersey',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'JE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 447,
            'uuid' => 'ff7abe3c-5374-bb92-52f0-43426d6e9941',
            'r_name' => 'Europe/Kaliningrad',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 448,
            'uuid' => '4d7f41ba-b495-b40b-2988-83d6c089b854',
            'r_name' => 'Europe/Kiev',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 449,
            'uuid' => '50625bb6-a8fb-7042-bfc0-7e743e233ac8',
            'r_name' => 'Europe/Kirov',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 450,
            'uuid' => 'cb1922e2-2f25-72d6-968b-a722d5b6e422',
            'r_name' => 'Europe/Lisbon',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 451,
            'uuid' => '724cb0b0-f4d7-409b-feb1-743a953089a3',
            'r_name' => 'Europe/Ljubljana',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 452,
            'uuid' => 'e217ab9e-b3ba-9973-2814-5bd7bbd7cdd8',
            'r_name' => 'Europe/London',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 453,
            'uuid' => 'd12d7f9b-621e-512b-ea6b-0c1045a26aed',
            'r_name' => 'Europe/Luxembourg',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 454,
            'uuid' => '0e8ac099-1eaf-2bce-87c9-9a5b0e624cbc',
            'r_name' => 'Europe/Madrid',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ES',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 455,
            'uuid' => '212e6855-abe0-cebd-8fd7-4ca040f4b268',
            'r_name' => 'Europe/Malta',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 456,
            'uuid' => 'db933dd0-dde0-158d-964e-c40ec97aa40e',
            'r_name' => 'Europe/Mariehamn',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 457,
            'uuid' => '58065f74-f44e-c3b4-65e6-6457deab724e',
            'r_name' => 'Europe/Minsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 458,
            'uuid' => '2381d05c-d553-ff42-547d-d8b200c4a563',
            'r_name' => 'Europe/Monaco',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 459,
            'uuid' => '7b2c1e6f-380b-070b-3372-83885014abd8',
            'r_name' => 'Europe/Moscow',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 460,
            'uuid' => 'f499a49c-00fd-d3b8-47f6-4e1373abcb40',
            'r_name' => 'Europe/Nicosia',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 461,
            'uuid' => 'fec03625-7f73-ed00-147a-4231c9d6af93',
            'r_name' => 'Europe/Oslo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 462,
            'uuid' => 'dba0bb7a-3b4f-b719-b77c-6369aeaf7dd7',
            'r_name' => 'Europe/Paris',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 463,
            'uuid' => 'cde74c20-affc-14cc-cc9d-74c4ac9455d1',
            'r_name' => 'Europe/Podgorica',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'ME',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 464,
            'uuid' => '1c4fbd8a-d7cd-a9ec-7a4e-e8ee22806e10',
            'r_name' => 'Europe/Prague',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 465,
            'uuid' => '033213b5-ac98-507a-2978-209b46acd0ac',
            'r_name' => 'Europe/Riga',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LV',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 466,
            'uuid' => '750eff41-7351-97b4-9925-c2f44b630520',
            'r_name' => 'Europe/Rome',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 467,
            'uuid' => '975d4855-5960-8229-aa3a-b44338fdbffe',
            'r_name' => 'Europe/Samara',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 468,
            'uuid' => '48a673b7-ac01-0879-2025-7dcba94f6f1a',
            'r_name' => 'Europe/San_Marino',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 469,
            'uuid' => 'f3d59c5a-19fe-2133-044d-a44e375c8438',
            'r_name' => 'Europe/Sarajevo',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 470,
            'uuid' => '75f2a001-cff3-6260-ed9e-b8128e07111f',
            'r_name' => 'Europe/Saratov',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 471,
            'uuid' => 'a600772d-e21a-a4dd-a534-304bfa4ee05d',
            'r_name' => 'Europe/Simferopol',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 472,
            'uuid' => '89146f3d-80ec-18d6-23c9-cbc413c3f912',
            'r_name' => 'Europe/Skopje',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 473,
            'uuid' => '3acdf0f7-9a01-f7f8-c253-e423fc7d4ebe',
            'r_name' => 'Europe/Sofia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'BG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 474,
            'uuid' => 'd7713bff-f039-e3f9-e371-520989b17de9',
            'r_name' => 'Europe/Stockholm',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 475,
            'uuid' => '5f6c3116-9e3b-a4c9-2717-450eed2494f0',
            'r_name' => 'Europe/Tallinn',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'EE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 476,
            'uuid' => 'b5cdd70a-382c-9232-2614-632cf9cae89b',
            'r_name' => 'Europe/Tirane',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 477,
            'uuid' => 'a1cba0d1-c11d-a8fe-b324-9d1d6e4275c5',
            'r_name' => 'Europe/Tiraspol',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MD',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 478,
            'uuid' => '35174fd9-fe21-7746-4390-4d0c8745769a',
            'r_name' => 'Europe/Ulyanovsk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 479,
            'uuid' => '9ba9dd0f-042d-9037-ce50-6e41e37bc737',
            'r_name' => 'Europe/Uzhgorod',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 480,
            'uuid' => '0e00f863-0c0e-e0d6-dde8-4123c9dcdb21',
            'r_name' => 'Europe/Vaduz',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 481,
            'uuid' => '02e18437-f844-e48d-49d2-2e9ab7493e74',
            'r_name' => 'Europe/Vatican',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 482,
            'uuid' => 'ebad7438-6894-1320-6e73-7572a16e5a7e',
            'r_name' => 'Europe/Vienna',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 483,
            'uuid' => '38aa4312-b422-ceb2-2aee-a432f8e4e7fa',
            'r_name' => 'Europe/Vilnius',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 484,
            'uuid' => '9ebd4e3d-0807-0335-3e6b-56d470b11a3f',
            'r_name' => 'Europe/Volgograd',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 485,
            'uuid' => '9b5a0618-1126-4aba-1f45-a5c123624d29',
            'r_name' => 'Europe/Warsaw',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 486,
            'uuid' => '256a939e-b2ba-a4f2-6765-9983fece7bf7',
            'r_name' => 'Europe/Zagreb',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'HR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 487,
            'uuid' => '300d281c-491a-8b99-56ea-22d43bd39a67',
            'r_name' => 'Europe/Zaporozhye',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UA',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 488,
            'uuid' => 'bcfacda1-2c15-5624-4c08-d6f3fa5fea28',
            'r_name' => 'Europe/Zurich',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 489,
            'uuid' => '52e4c0e5-1255-bdc7-7d47-91ce45bcd75e',
            'r_name' => 'Factory',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 490,
            'uuid' => 'fb0c682c-a4c5-ec27-b47e-91b71581b621',
            'r_name' => 'GB',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 491,
            'uuid' => '03f95a20-c156-36a3-ae0c-2f2989375fbb',
            'r_name' => 'GB-Eire',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 492,
            'uuid' => 'f49ec1f1-50f9-d65f-c44c-6eef57d8f6e5',
            'r_name' => 'GMT',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 493,
            'uuid' => '4af552f5-4e3d-63be-b6ff-5a184657a87b',
            'r_name' => 'GMT+0',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 494,
            'uuid' => '6cf92bad-1cf2-0900-1276-60bb49974a94',
            'r_name' => 'GMT-0',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 495,
            'uuid' => '678b54f9-e108-2b91-90fd-2dd38afe368c',
            'r_name' => 'GMT0',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 496,
            'uuid' => '2f6b6ccd-ff7d-9cc6-7474-d08c9d8f33e0',
            'r_name' => 'Greenwich',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 497,
            'uuid' => '8fae5b25-6368-393b-6530-d9eaa0dfcc50',
            'r_name' => 'Hongkong',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'HK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 498,
            'uuid' => '45c0b46c-c330-9998-b072-3ba0ef70b74d',
            'r_name' => 'HST',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 499,
            'uuid' => '3dfede5d-f23c-87ee-0fe2-b7e810422f72',
            'r_name' => 'Iceland',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 500,
            'uuid' => '90bc3f8f-972f-819d-336c-3e4889be1101',
            'r_name' => 'Indian/Antananarivo',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 501,
            'uuid' => '4e79199b-9dc2-825e-2123-2da74bbe9dcb',
            'r_name' => 'Indian/Chagos',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 21600,
            'dst_l10n_datetime_utc_offset_seconds' => 21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 502,
            'uuid' => '418043dc-41db-141b-ee3c-2a55c4ab8137',
            'r_name' => 'Indian/Christmas',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 25200,
            'dst_l10n_datetime_utc_offset_seconds' => 25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 503,
            'uuid' => 'e4113d23-d019-d634-8907-4f76ec9210bb',
            'r_name' => 'Indian/Cocos',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 23400,
            'dst_l10n_datetime_utc_offset_seconds' => 23400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 504,
            'uuid' => '1840eaac-d69a-0e7e-723a-ce5bef5756d1',
            'r_name' => 'Indian/Comoro',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 505,
            'uuid' => '3128c98e-406a-a99f-bf06-d7da197956e8',
            'r_name' => 'Indian/Kerguelen',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 506,
            'uuid' => 'b55d0b6f-b139-f726-18a5-69bf81e9c5f7',
            'r_name' => 'Indian/Mahe',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 507,
            'uuid' => '2b9558b8-4f6c-83f3-1bd2-b1eb3d1e3828',
            'r_name' => 'Indian/Maldives',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 18000,
            'dst_l10n_datetime_utc_offset_seconds' => 18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MV',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 508,
            'uuid' => 'b91a7326-79b3-2c87-f876-219ee58c9b71',
            'r_name' => 'Indian/Mauritius',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 509,
            'uuid' => 'd8d2469d-2133-a2da-fa98-de91329b0a08',
            'r_name' => 'Indian/Mayotte',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'YT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 510,
            'uuid' => '2d422a7f-5ad8-f2f2-4ff1-ebdec27711ee',
            'r_name' => 'Indian/Reunion',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 14400,
            'dst_l10n_datetime_utc_offset_seconds' => 14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RE',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 511,
            'uuid' => 'c3225cc9-4a02-f83b-99e5-21176ee295e3',
            'r_name' => 'Iran',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 12600,
            'dst_l10n_datetime_utc_offset_seconds' => 16200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 512,
            'uuid' => '3b2903ea-0f59-0b90-910f-71929267ce5a',
            'r_name' => 'Israel',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'IL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 513,
            'uuid' => 'd910b1f0-a6c4-5c77-4773-1284358203df',
            'r_name' => 'Jamaica',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'JM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 514,
            'uuid' => '5574160d-6ea7-6003-f83c-1daa1a3a3236',
            'r_name' => 'Japan',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'JP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 515,
            'uuid' => 'a44f6b82-7f2d-6c6b-8261-b5b51443a6ec',
            'r_name' => 'Kwajalein',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 516,
            'uuid' => '4b191914-aba4-b19d-141d-2c3b7754eea7',
            'r_name' => 'Libya',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 7200,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'LY',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 517,
            'uuid' => '40dae92a-6cd0-b5d8-5791-11487323ccd5',
            'r_name' => 'MET',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 518,
            'uuid' => 'c0e61d48-f141-7c67-587d-3c2563e3c696',
            'r_name' => 'Mexico/BajaNorte',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 519,
            'uuid' => '41ec02df-1e3c-2539-6b58-cf9c5bab6ff2',
            'r_name' => 'Mexico/BajaSur',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 520,
            'uuid' => '7a85542d-2d7e-83b8-88a5-9e6ee913b109',
            'r_name' => 'Mexico/General',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MX',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 521,
            'uuid' => '87b96da5-cb21-5fb3-bceb-2fe85a4fb3dd',
            'r_name' => 'MST',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 522,
            'uuid' => 'a9f49bf0-3e11-0482-907a-581026a825a5',
            'r_name' => 'MST7MDT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 523,
            'uuid' => '0fbecaa0-d9cb-b86e-9204-950e07bd05db',
            'r_name' => 'Navajo',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 524,
            'uuid' => 'ba07159b-c29d-e8e1-0a84-e35cdf9442b3',
            'r_name' => 'NZ',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 525,
            'uuid' => '3fc22123-ede9-f965-a303-e05363e084b8',
            'r_name' => 'NZ-CHAT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 45900,
            'dst_l10n_datetime_utc_offset_seconds' => 49500,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 526,
            'uuid' => 'dd93d457-3fc4-7efc-bed8-ad2e78565c05',
            'r_name' => 'Pacific/Apia',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 46800,
            'dst_l10n_datetime_utc_offset_seconds' => 50400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'WS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 527,
            'uuid' => 'b0d05c08-f8f3-9157-f388-00a3e23e6d02',
            'r_name' => 'Pacific/Auckland',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 528,
            'uuid' => '2e159e1f-b5ec-b544-c02c-fc9e43e12751',
            'r_name' => 'Pacific/Bougainville',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 529,
            'uuid' => 'a49ad713-083b-e8f6-95cb-7baae39e85ba',
            'r_name' => 'Pacific/Chatham',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 45900,
            'dst_l10n_datetime_utc_offset_seconds' => 49500,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NZ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 530,
            'uuid' => '47fb1398-f1b0-f725-c446-c0f9bfde137c',
            'r_name' => 'Pacific/Chuuk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 531,
            'uuid' => '7844fc5a-91e9-a673-1a98-d893f9d74311',
            'r_name' => 'Pacific/Easter',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 532,
            'uuid' => 'fbe9c540-8c94-b4a3-4a45-9b67bd7bd55c',
            'r_name' => 'Pacific/Efate',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'VU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 533,
            'uuid' => '29ff42da-444a-ec1e-4f16-2437947f782d',
            'r_name' => 'Pacific/Enderbury',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 46800,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 534,
            'uuid' => '261d02b3-aa20-258a-1ef5-e5ebd03762a3',
            'r_name' => 'Pacific/Fakaofo',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 46800,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 535,
            'uuid' => '94e4710e-a7f2-6b5f-0aa7-3aa2df86aa53',
            'r_name' => 'Pacific/Fiji',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FJ',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 536,
            'uuid' => 'cf232dbd-4593-6241-adaf-34d74af52092',
            'r_name' => 'Pacific/Funafuti',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TV',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 537,
            'uuid' => '3a6124c9-aa52-7c4c-0a31-083a8e5b1bc7',
            'r_name' => 'Pacific/Galapagos',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'EC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 538,
            'uuid' => '83df7bb2-f6c2-d42d-855c-b17c41a30996',
            'r_name' => 'Pacific/Gambier',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 539,
            'uuid' => '447799d5-176a-eaee-60df-9b5e2b7d42e6',
            'r_name' => 'Pacific/Guadalcanal',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SB',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 540,
            'uuid' => 'f9029163-fdf3-c4f9-7726-2a50ba3c588e',
            'r_name' => 'Pacific/Guam',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'GU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 541,
            'uuid' => 'd9ca3162-e8c0-4b2e-1698-cc9a32a76e52',
            'r_name' => 'Pacific/Honolulu',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 542,
            'uuid' => '228393dc-1bbc-2167-3405-e22ff1ef109a',
            'r_name' => 'Pacific/Johnston',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 543,
            'uuid' => '870f1372-141b-dfd0-905c-833578f680c2',
            'r_name' => 'Pacific/Kiritimati',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 50400,
            'dst_l10n_datetime_utc_offset_seconds' => 50400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 544,
            'uuid' => 'bbfd4e74-ed3c-e7f3-8b9e-1c996c110e76',
            'r_name' => 'Pacific/Kosrae',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 545,
            'uuid' => '7c3e105a-97c2-1115-ae45-6041e367bf2c',
            'r_name' => 'Pacific/Kwajalein',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 546,
            'uuid' => '7a0c9ea1-552a-ed8e-00cf-0a3535681aa3',
            'r_name' => 'Pacific/Majuro',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MH',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 547,
            'uuid' => 'a6949583-610a-3915-e377-ab1c05912378',
            'r_name' => 'Pacific/Marquesas',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -34200,
            'dst_l10n_datetime_utc_offset_seconds' => -34200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 548,
            'uuid' => '1875d975-9077-6003-53de-d0f40b6ebb5f',
            'r_name' => 'Pacific/Midway',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -39600,
            'dst_l10n_datetime_utc_offset_seconds' => -39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 549,
            'uuid' => 'fc26b883-1917-c37f-1d14-469bd99e8bd5',
            'r_name' => 'Pacific/Nauru',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 550,
            'uuid' => 'b11fb742-fe78-830b-c478-21f4a09dbb89',
            'r_name' => 'Pacific/Niue',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -39600,
            'dst_l10n_datetime_utc_offset_seconds' => -39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 551,
            'uuid' => 'f06dc555-b929-7357-3274-85f36917251a',
            'r_name' => 'Pacific/Norfolk',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 552,
            'uuid' => '54c12692-1690-74d8-d861-fe6e9cba39de',
            'r_name' => 'Pacific/Noumea',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'NC',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 553,
            'uuid' => 'c7bd8f79-0104-4784-d182-91a5bef449db',
            'r_name' => 'Pacific/Pago_Pago',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -39600,
            'dst_l10n_datetime_utc_offset_seconds' => -39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'AS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 554,
            'uuid' => '9a459164-8678-17b5-9096-6495a60c55a5',
            'r_name' => 'Pacific/Palau',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 555,
            'uuid' => 'cb5291db-37de-f118-4971-dcb080b88fd5',
            'r_name' => 'Pacific/Pitcairn',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 556,
            'uuid' => 'f98b8b4b-b72a-181d-9ff8-3437cf9ea37b',
            'r_name' => 'Pacific/Pohnpei',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 557,
            'uuid' => '67a462be-3e4c-a7d8-d435-f518c2f4d8e4',
            'r_name' => 'Pacific/Ponape',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 39600,
            'dst_l10n_datetime_utc_offset_seconds' => 39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 558,
            'uuid' => '06078841-e3ee-8636-5339-0b893c75db1a',
            'r_name' => 'Pacific/Port_Moresby',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 559,
            'uuid' => 'a415bcef-03b5-effa-3daa-411a4cdd13a2',
            'r_name' => 'Pacific/Rarotonga',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CK',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 560,
            'uuid' => '720304f6-ac35-6721-cca2-ba6c1c95b7ed',
            'r_name' => 'Pacific/Saipan',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'MP',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 561,
            'uuid' => '8186e53b-24a7-c5cf-b654-aa2156df1db4',
            'r_name' => 'Pacific/Samoa',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -39600,
            'dst_l10n_datetime_utc_offset_seconds' => -39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'WS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 562,
            'uuid' => '1dac8264-e543-92b1-1a95-354a7923b544',
            'r_name' => 'Pacific/Tahiti',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 563,
            'uuid' => '4ceb630b-6db9-3c40-3cec-e383e9174467',
            'r_name' => 'Pacific/Tarawa',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KI',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 564,
            'uuid' => '56b72929-0c7b-61e2-3a3d-acd98d85eac4',
            'r_name' => 'Pacific/Tongatapu',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 46800,
            'dst_l10n_datetime_utc_offset_seconds' => 46800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TO',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 565,
            'uuid' => 'aa17b377-ca6b-c36d-c052-5362f13cb8b9',
            'r_name' => 'Pacific/Truk',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 566,
            'uuid' => '0d60b94d-84c4-a870-202f-ebeac8bc3132',
            'r_name' => 'Pacific/Wake',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'UM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 567,
            'uuid' => 'cf59830b-1ea3-d790-fab6-d01afecb1ebc',
            'r_name' => 'Pacific/Wallis',
            'r_status' => 1,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 43200,
            'dst_l10n_datetime_utc_offset_seconds' => 43200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'WF',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 568,
            'uuid' => '68f4cd3d-6b2c-6b73-5438-585d3ab3045b',
            'r_name' => 'Pacific/Yap',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 36000,
            'dst_l10n_datetime_utc_offset_seconds' => 36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'FM',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 569,
            'uuid' => 'f7524442-c69e-c5c3-9bf4-c36d0680edaf',
            'r_name' => 'Poland',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 3600,
            'dst_l10n_datetime_utc_offset_seconds' => 7200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PL',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 570,
            'uuid' => '3d8cd610-2952-57f8-33ed-8efe753c8da9',
            'r_name' => 'Portugal',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'PT',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 571,
            'uuid' => '04888b29-8e68-37b9-ddd7-349108a2b002',
            'r_name' => 'PRC',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'CN',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 572,
            'uuid' => 'b7b9bb44-6baa-8adc-d5dd-87e2d803d714',
            'r_name' => 'PST8PDT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 573,
            'uuid' => '72519b2c-25d6-7022-c442-a4789291a822',
            'r_name' => 'ROC',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TW',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 574,
            'uuid' => 'ec0b25c3-9baf-8386-8964-1db1b367694d',
            'r_name' => 'ROK',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 32400,
            'dst_l10n_datetime_utc_offset_seconds' => 32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'KR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 575,
            'uuid' => 'de2f6e9d-1413-a1db-9b18-02bf19f51d5c',
            'r_name' => 'Singapore',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 28800,
            'dst_l10n_datetime_utc_offset_seconds' => 28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'SG',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 576,
            'uuid' => 'f8500bba-c3f3-0ea9-72e0-20ecf38a84d7',
            'r_name' => 'Turkey',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'TR',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 577,
            'uuid' => 'a4b3ada9-c7d6-5741-7f3f-3396e9a98d41',
            'r_name' => 'UCT',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 578,
            'uuid' => '76a5ecb6-9ccc-01d4-7f55-0528a4317d5c',
            'r_name' => 'Universal',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 579,
            'uuid' => 'c2676f68-457c-cf3a-4baa-9acb7ab078cb',
            'r_name' => 'US/Alaska',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -32400,
            'dst_l10n_datetime_utc_offset_seconds' => -28800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 580,
            'uuid' => 'fdc07568-8996-3bd4-f9f0-3d5743ffbdf7',
            'r_name' => 'US/Aleutian',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -32400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 581,
            'uuid' => '1ecc80a4-a229-87b0-8c90-a2f76e84cf4b',
            'r_name' => 'US/Arizona',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 582,
            'uuid' => 'be4d0da6-cf69-2d26-6d3e-854d66478cec',
            'r_name' => 'US/Central',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 583,
            'uuid' => 'ae77382b-21d2-1057-9c4d-bc55cac20d28',
            'r_name' => 'US/East-Indiana',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 584,
            'uuid' => 'cf36ba4c-2f2e-dd3e-cb18-7e64f6c3de36',
            'r_name' => 'US/Eastern',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 585,
            'uuid' => 'c5b18a48-4ff0-6067-0e95-239f28791f6a',
            'r_name' => 'US/Hawaii',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -36000,
            'dst_l10n_datetime_utc_offset_seconds' => -36000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 586,
            'uuid' => '24a5da78-2346-46b0-9413-5a0a156bcfba',
            'r_name' => 'US/Indiana-Starke',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -21600,
            'dst_l10n_datetime_utc_offset_seconds' => -18000,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 587,
            'uuid' => '6e54536b-8f99-64a3-2f52-aabb17407e46',
            'r_name' => 'US/Michigan',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -18000,
            'dst_l10n_datetime_utc_offset_seconds' => -14400,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 588,
            'uuid' => 'f0401f77-4b2b-d637-524a-4c3de040de61',
            'r_name' => 'US/Mountain',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -25200,
            'dst_l10n_datetime_utc_offset_seconds' => -21600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 589,
            'uuid' => 'ecab50de-8ef1-24f4-e2ad-17207f9a8032',
            'r_name' => 'US/Pacific',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -28800,
            'dst_l10n_datetime_utc_offset_seconds' => -25200,
            'i18n_territorial_division_code_iso3166_alpha2' => 'US',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 590,
            'uuid' => '6255ce6f-6f22-891c-d7c5-3813293adace',
            'r_name' => 'US/Samoa',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => -39600,
            'dst_l10n_datetime_utc_offset_seconds' => -39600,
            'i18n_territorial_division_code_iso3166_alpha2' => 'WS',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 591,
            'uuid' => '76be53ec-0028-2b4e-d4a3-3420020dc929',
            'r_name' => 'UTC',
            'r_status' => 2,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 592,
            'uuid' => 'fe486fbe-a790-c951-e4d8-07bb53addc3c',
            'r_name' => 'W-SU',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 10800,
            'dst_l10n_datetime_utc_offset_seconds' => 10800,
            'i18n_territorial_division_code_iso3166_alpha2' => 'RU',
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 593,
            'uuid' => 'a985bb0e-1a00-d895-de82-bdadd74f8074',
            'r_name' => 'WET',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 3600,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
        $this->connection->insert('l10n_datetime_time_zone', [
            'id' => 594,
            'uuid' => '7a2f42a4-69b5-55e6-7ebe-8b2ec93c2edd',
            'r_name' => 'Zulu',
            'r_status' => 0,
            'links_to_l10n_datetime_time_zone_id' => null,
            'l10n_datetime_utc_offset_seconds' => 0,
            'dst_l10n_datetime_utc_offset_seconds' => 0,
            'i18n_territorial_division_code_iso3166_alpha2' => null,
        ]);
    }

    public function down(Schema $schema): void
    {

    }
}
