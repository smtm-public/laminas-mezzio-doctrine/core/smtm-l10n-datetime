<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

class UtcOffsetExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'hours' => null,
        'seconds' => null,
    ];
}
