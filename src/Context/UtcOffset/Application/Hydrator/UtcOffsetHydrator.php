<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class UtcOffsetHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'hours' => 'You must specify hours for the UtcOffset.',
        'seconds' => 'You must specify seconds for the UtcOffset.',
    ];
}
