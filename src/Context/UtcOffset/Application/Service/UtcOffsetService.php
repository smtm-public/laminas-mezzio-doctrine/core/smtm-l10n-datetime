<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbConfigurableEntityManagerNameServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\ConfigAwareTrait;
use Smtm\L10n\DateTime\Context\UtcOffset\Application\Hydrator\UtcOffsetHydrator;
use Smtm\L10n\DateTime\Context\UtcOffset\Domain\UtcOffset;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UtcOffsetService extends AbstractDbService implements
    UuidAwareEntityDbServiceInterface, DbConfigurableEntityManagerNameServiceInterface
{

    use UuidAwareEntityDbServiceTrait, ConfigAwareTrait;

    protected ?string $domainObjectName = UtcOffset::class;
    protected ?string $hydratorName = UtcOffsetHydrator::class;
}
