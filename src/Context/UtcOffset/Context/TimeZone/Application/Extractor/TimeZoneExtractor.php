<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

class TimeZoneExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'name' => null,
        'i18nTerritorialDivisionCodeIso3166Alpha2' => null,
        'l10nDateTimeUtcOffsetSeconds' => null,
        'dstL10nDateTimeUtcOffsetSeconds' => null,
    ];
}
