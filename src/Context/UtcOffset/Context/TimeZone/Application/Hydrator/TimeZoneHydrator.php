<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class TimeZoneHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'name' => 'You must specify a name for the TimeZone.',
        'i18nTerritorialDivisionCodeIso3166Alpha2' => 'You must specify a i18nTerritorialDivisionCodeIso3166Alpha2 for the TimeZone.',
        'l10nDateTimeUtcOffsetSeconds' => 'You must specify a l10nDateTimeUtcOffsetSeconds for the TimeZone.',
        'dstL10nDateTimeUtcOffsetSeconds' => 'You must specify a dstL10nDateTimeUtcOffsetSeconds for the TimeZone.',
    ];
}
