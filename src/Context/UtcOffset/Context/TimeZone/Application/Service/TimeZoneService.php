<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbConfigurableEntityManagerNameServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\ConfigAwareTrait;
use Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Hydrator\TimeZoneHydrator;
use Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Domain\TimeZone;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TimeZoneService extends AbstractDbService implements
    UuidAwareEntityDbServiceInterface, DbConfigurableEntityManagerNameServiceInterface
{

    use UuidAwareEntityDbServiceTrait, ConfigAwareTrait;

    protected ?string $domainObjectName = TimeZone::class;
    protected ?string $hydratorName = TimeZoneHydrator::class;
}
