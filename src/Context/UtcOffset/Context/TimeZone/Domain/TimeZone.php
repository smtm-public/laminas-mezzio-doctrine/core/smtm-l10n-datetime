<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Domain;

use DateTimeInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityInterface;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityTrait;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TimeZone implements
    IdAwareEntityInterface,
    UuidAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{

    use IdAwareEntityTrait;
    use UuidAwareEntityTrait {
        __construct as traitConstructor;
    }
    use NotArchivedAwareEntityTrait, ArchivedDateTimeAwareEntityTrait;

    protected string $name;
    protected ?string $i18nTerritorialDivisionCodeIso3166Alpha2 = null;
    protected int $l10nDateTimeUtcOffsetSeconds;
    protected int $dstL10nDateTimeUtcOffsetSeconds;

    public function __construct(string $timezone)
    {
        //parent::__construct($timezone);

        $this->traitConstructor();
    }

//    public function __postLoad()
//    {
//        parent::__construct($this->name);
//    }

    public function getName(): string
    {
        //return parent::getName();
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getI18nTerritorialDivisionCodeIso3166Alpha2(): ?string
    {
        return $this->i18nTerritorialDivisionCodeIso3166Alpha2;
    }

    public function setI18nTerritorialDivisionCodeIso3166Alpha2(
        ?string $i18nTerritorialDivisionCodeIso3166Alpha2
    ): static {
        $this->i18nTerritorialDivisionCodeIso3166Alpha2 = $i18nTerritorialDivisionCodeIso3166Alpha2;

        return $this;
    }

    public function getL10nDateTimeUtcOffsetSeconds(): int
    {
        return $this->l10nDateTimeUtcOffsetSeconds;
    }

    public function setL10nDateTimeUtcOffsetSeconds(int $l10nDateTimeUtcOffsetSeconds): static
    {
        $this->l10nDateTimeUtcOffsetSeconds = $l10nDateTimeUtcOffsetSeconds;

        return $this;
    }

    public function getDstL10nDateTimeUtcOffsetSeconds(): int
    {
        return $this->dstL10nDateTimeUtcOffsetSeconds;
    }

    public function setDstL10nDateTimeUtcOffsetSeconds(int $dstL10nDateTimeUtcOffsetSeconds): static
    {
        $this->dstL10nDateTimeUtcOffsetSeconds = $dstL10nDateTimeUtcOffsetSeconds;

        return $this;
    }

    public function getInternalObject(): \DateTimeZone
    {
        return new \DateTimeZone($this->getName());
    }

    public function getOffset(DateTimeInterface $datetime): int
    {
        return $this->getInternalObject()->getOffset($datetime);
    }

    public function getTransitions(int $timestampBegin = null, int $timestampEnd = null): array|false
    {
        return $this->getInternalObject()->getTransitions($timestampBegin, $timestampEnd);
    }

    #[ArrayShape([
        'country_code' => 'string',
        'latitude' => 'double',
        'longitude' => 'double',
        'comments' => 'string',
    ])] public function getLocation(): array|false
    {
        return $this->getInternalObject()->getLocation();
    }

//    public function __wakeup(): void
//    {
//        /*
//         * TODO: MD - This breaks Doctrine's proxy logic, which calls the __wakeup method before the call to
//         *       \Doctrine\ORM\Persisters\Entity\EntityPersister::loadById() (after which the postLoad event is
//         *       triggered) so the parent \DateTimeZone is never properly initialized.
//         */
//        if (ReflectionHelper::isPropertyInitialized('name', $this)) {
//            parent::__wakeup();
//        }
//    }
}
