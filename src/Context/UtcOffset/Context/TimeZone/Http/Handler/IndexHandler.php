<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Http\Handler;

use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractIndexHandler;
use Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Extractor\TimeZoneExtractor;
use Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Application\Service\TimeZoneService;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IndexHandler extends AbstractIndexHandler
{
    public ?string $applicationServiceName = TimeZoneService::class;
    public ?string $domainObjectExtractorName = TimeZoneExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        $mergedFilteredQueryParams = static::getMergedFilteredQueryParams($request);
        $parsedQueryParams = static::parseQueryParams($mergedFilteredQueryParams);

        $timeZones = $this->getAll($request);

        return $this->prepareResponse($timeZones, $parsedQueryParams);

    }
}
