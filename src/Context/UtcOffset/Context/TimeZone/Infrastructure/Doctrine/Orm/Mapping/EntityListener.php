<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Infrastructure\Doctrine\Orm\Mapping;

use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\AbstractEntityListener;
use Doctrine\ORM\Event\PostLoadEventArgs;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityListener extends AbstractEntityListener
{
    public function postLoad(EntityInterface $entity, PostLoadEventArgs $args): void
    {
        $entity->__postLoad();
    }
}
