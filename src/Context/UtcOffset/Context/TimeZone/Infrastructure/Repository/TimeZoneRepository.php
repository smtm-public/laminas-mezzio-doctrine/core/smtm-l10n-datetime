<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Context\TimeZone\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TimeZoneRepository extends AbstractRepository implements
    TimeZoneRepositoryInterface
{

}
