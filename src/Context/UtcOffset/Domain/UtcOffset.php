<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Domain;

use Smtm\Base\Domain\AbstractUuidAwareEntity;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UtcOffset extends AbstractUuidAwareEntity
{
    protected string $hours;
    protected int $seconds;

    public function getHours(): string
    {
        return $this->hours;
    }

    public function setHours(string $hours): static
    {
        $this->hours = $hours;

        return $this;
    }

    public function getSeconds(): int
    {
        return $this->seconds;
    }

    public function setSeconds(int $seconds): static
    {
        $this->seconds = $seconds;

        return $this;
    }
}
