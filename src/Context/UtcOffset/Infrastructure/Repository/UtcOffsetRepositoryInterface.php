<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Context\UtcOffset\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UtcOffsetRepositoryInterface extends RepositoryInterface
{

}
