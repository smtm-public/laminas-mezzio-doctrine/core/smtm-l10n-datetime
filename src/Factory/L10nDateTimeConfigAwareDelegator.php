<?php

declare(strict_types=1);

namespace Smtm\L10n\DateTime\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class L10nDateTimeConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = ['l10n', 'datetime'];
}
